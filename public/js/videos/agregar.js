$(function(){
    reproductor=$("#reproductor")
});
function reproducirVideo(url)
{
    reproductor.html('<a href="'+url+'" style="display:block;width:100%;height:100%;" id="player"> </a> <script> flowplayer("player", "'+baseUrl+'js/flowplayer/flowplayer-3.2.12.swf");</script>');
}

$(document).ready(function(){
    var input = document.getElementById('video'),
    formdata = new FormData();
    
    //Aplicamos la subida de imágenes al evento change del input file
    if(input.addEventListener){
        input.addEventListener('change', function(evt){
            var i = 0, len = this.files.length, reader, file;
                         
            //Si hay varias imágenes, las obtenemos una a una
            for( ; i < len; i++){
                file = this.files[i];
                
                //Una pequeña validación para subir imágenes
                
                //Si el navegador soporta el objeto FileReader
                if(window.FileReader){
                    reader = new FileReader();
                    //Llamamos a este evento cuando la lectura del archivo es completa
                    //Después agregamos la imagen en una lista
                    reader.onloadend = function(e){
                           
                    };
                    //Comienza a leer el archivo
                    //Cuando termina el evento onloadend es llamado
                    reader.readAsDataURL(file);
                }
                    
                //Si existe una instancia de FormData
                if(formdata)
                    //Usamos el método append, cuyos parámetros son:
                    //name : El nombre del campo
                    //value: El valor del campo (puede ser de tipo Blob, File e incluso string)
                    formdata.append('images[]', file);
               
            }
            
            //Por último hacemos uso del método proporcionado por jQuery para hacer la petición ajax
            //Como datos a enviar, el objeto FormData que contiene la información de las imágenes
            if(formdata){
                $.ajax({
                    url : baseUrl+'php/upload.php?ruta=../uploads/videos/&tipo=video',
                    type : 'POST',
                    data : formdata,
                    processData : false, 
                    contentType : false, 
                    beforeSend : function(){
                        $().toastmessage('showNoticeToast',"Subiendo archivo");		
                    },
                    success : function(res){
                        document.getElementById('video').value=null;
                        if(res!="")
                        {
                            $().toastmessage('showSuccessToast',"Imagen guardada como "+res);
                            location.href=location.href;
                        }
                    }                
                });
            }
        }, false);
    }
});