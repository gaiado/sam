$(function(){
    esquema=$("#idesquemas").change(cambiarEsquema);
    divEsquema=$("#esquema");
    cagarEsquemaInicio();
});

function cambiarEsquema()
{
    $.post(baseUrl+"esquema/cargar/idesquemas/"+this.value,function(data){
        if(data!="")
        {
            divEsquema.load(baseUrl+data+"index.php");            
        }
    });
}
function cagarEsquemaInicio()
{
    $.post(baseUrl+'esquema/cargar/idesquemas/'+esquema.attr("value"), function(data) {
        if(data!="")
            divEsquema.load(baseUrl+data+"index.php");
    });
}