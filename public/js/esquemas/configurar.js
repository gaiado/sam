$(function () {
    divPlantilla = $("#divPlantilla");
    cagarPlantillaInicio();
    selecionado = $("#divselec");
    numdiv = $("#numdiv");
    cantidad = $("#cantidad").attr("value");
});
function cagarPlantillaInicio()
{
    $.post(baseUrl + 'template/cargar/id/' + divPlantilla.text(), function (data) {
        if (data != "")
            divPlantilla.load(baseUrl + data + "index.php");
    });
}
function actualizarDiv(tipomodulo, idmodulo)
{
    var divselec = selecionado.attr("value");
    var datos = divselec + '::' + tipomodulo + "::" + idmodulo;
    $.post(baseUrl + 'div/actualizar/datos/' + datos, function (data) {
        if (data != "")
            $("#div" + numdiv.attr("value")).load(baseUrl + "modulo/index/?modulo=" + tipomodulo + "&idmodulo=" + idmodulo);
    });
}
function selDiv(div) {
    for (var i = 1; i <= cantidad; i++)
    {
        $("#div" + i).attr("class", "div-no-selected");
    }
    $("#div" + div).attr("class", "div-selected");
}