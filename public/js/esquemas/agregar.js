$(function () {
    derecha = $("#derecha");
    plantilla = $("#idtemplates").change(cagarPlantilla);
    cagarPlantillaInicio();
});
function cagarPlantilla() {
    $.post(baseUrl + 'template/cargar/id/' + this.value, function (data) {
        if (data != "")
            derecha.load(baseUrl+data + "index.php");
    });
}
function cagarPlantillaInicio() {
    $.post(baseUrl + 'template/cargar/id/' + plantilla.attr("value"), function (data) {
        if (data != "")
            derecha.load(baseUrl + data + "index.php");
    });
}