<style type="text/css">
    #plantilla1{
        margin: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
    }
    #plantilla1 div{
        width: 50%;
        height: 50%;
    }
    #plantilla1 #div1{
        float: left;
        background-color: #D14;
    }
    #plantilla1 #div2{
        float: left;
        background-color: #005580;
    }
    #plantilla1 #div3{
        float: left;
        background-color: #1e347b;
    }
    #plantilla1 #div4{
        float: right;
        background-color: #f89406;
    }
    #plantilla1 h1
    {
        color: white;
    }
</style>
<div id="plantilla1">
    <div id="div1">
        <h1>1</h1>
    </div>
    <div id="div2">
        <h1>2</h1>
    </div>
    <div id="div3">
        <h1>3</h1>
    </div>
    <div id="div4">
        <h1>4</h1>
    </div>
</div>