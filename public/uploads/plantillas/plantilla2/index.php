<style type="text/css">
    #plantilla2{
        margin: 0;
        overflow: hidden;
        width: 100%;
        height: 100%;
    }
    #plantilla2 #div1{
        width: 100%;
        background-color: #D14;
        height: 10%;
    }
    #plantilla2 #div2{
        width: 70%;
        float: left;
        height: 80%;
        background-color: #149bdf;
    }
    #plantilla2 #div3{
        width: 30%;
        float: left;
        height: 40%;
        background-color: #005580;
    }
    #plantilla2 #div4{
        width: 30%;
        float: right;
        height: 40%;
        background-color: #1e347b;
    }
    #plantilla2 #div5{
        width: 100%;
        float: right;
        height: 10%;
        background-color: #f89406;
    }
    #plantilla2 h1
    {
        color: white;
    }
</style>
<div id="plantilla2">
    <div id="div1">
        <h1>1</h1>
    </div>
    <div id="div2">
        <h1>2</h1>
    </div>
    <div id="div3">
        <h1>3</h1>
    </div>
    <div id="div4">
        <h1>4</h1>
    </div>
    <div id="div5">
        <h1>5</h1>
    </div>
</div>
