<?php

$ruta = $_GET['ruta'];
if (isset($_GET['tipo'])) {
    $tipo = $_GET['tipo'];
}

foreach ($_FILES['images']['error'] as $key => $error) {
    if ($error == UPLOAD_ERR_OK) {
        $name = cambiar_url($_FILES['images']['name'][$key]);
        switch ($tipo) {
            case 'video':$name.="." .substr($name, -3);
                break;
            case 'image':$name = date("d-m-Y", time()) . rand() . substr($name, -4);
                break;
        }
        move_uploaded_file($_FILES['images']['tmp_name'][$key], $ruta . $name);
    }
}

function cambiar_url($url) {
    $url = strtolower($url);
    $buscar = array(' ', '&', '+');
    $url = str_replace($buscar, '-', $url);
    $buscar = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
    $remplzr = array('a', 'e', 'i', 'o', 'u', 'n');
    $url = str_replace($buscar, $remplzr, $url);
    $buscar = array('/[^a-z0-9-<>]/', '/[-]+/', '/<[^>]*>/');
    $remplzr = array('', '-',' ');
    $url = preg_replace($buscar, $remplzr, $url);
    return $url;
}

echo $name;
?>