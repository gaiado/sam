<?php

class Application_View_Helper_Instituciones extends Zend_View_Helper_Abstract {

    public function instituciones($id = null) {
        $model = new Application_Model_Instituciones();
        if (is_null($id)) {
            return $model->getAll();
        } else {
            $id = (int) $id;
            $row = $model->find($id)->current();
            return $row;
        }
    }

}