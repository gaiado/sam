<?php

class Application_Model_Videos extends Zend_Db_Table_Abstract {

    protected $_name = 'videos';
    protected $_primary = 'idvideos';

    public function getAll() {
        return $this->fetchAll();
    }

    public function archivos() {
        $carpeta = "uploads/videos";
        $ArrFicheros = scandir($carpeta);
        $archivos = array();
        foreach ($ArrFicheros as $value) {
            if ($value != "." && $value != "..")
                $archivos[$value] = $value;
        }
        return $archivos;
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $row->setFromArray($data);
        $row->save();
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function getRowByModulo($idmodulovideos) {
        $select = $this->select()->where('idmodulovideos = ?', $idmodulovideos);
        return $this->fetchAll($select);
    }

}