<?php

class Application_Model_Youtube extends Zend_Db_Table_Abstract {

    protected $_name = 'youtubes';
    protected $_primary = 'idyoutubes';

    public function getAll() {
        return $this->fetchAll();
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $row->setFromArray($data);
        $row->save();
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function getRowByModulo($idmodulo) {
        $select = $this->select()->where('idmoduloyoutubes = ?', $idmodulo);
        return $this->fetchAll($select);
    }

}
