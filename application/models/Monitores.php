<?php

class Application_Model_Monitores extends Zend_Db_Table_Abstract {

    protected $_name = 'monitores';
    protected $_primary = 'idmonitores';

    public function getAll() {
        return $this->fetchAll();
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
        $row->setFromArray($data);
        $row->save();
    }
    public function saveAuthLess($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $row->setFromArray($data);
        $row->save();
    }

    public function getRowbyIp($ip) {
        $select = $this->select()->from(array("m" => "monitores"), array("idmonitores", "ip", "refrescar","fecharegistro"))
                ->join(array("e" => "esquemas"), "m.idesquemas=e.idesquemas", array())
                ->join(array("c" => "configuraciones"), "e.idconfiguraciones=c.idconfiguraciones", array("idconfiguraciones"))
                ->join(array("t" => "templates"), "t.idtemplates=c.idtemplates", array("url", "numdiv"))
                ->where('ip = ?', $ip)
                ->setIntegrityCheck(false);
        return $this->fetchAll($select)->current();
    }

    public function getRowById($id) {
        $select = $this->select()->from(array("m" => "monitores"), array("idmonitores", "ip", "refrescar","fecharegistro"))
                ->join(array("e" => "esquemas"), "m.idesquemas=e.idesquemas", array())
                ->join(array("c" => "configuraciones"), "e.idconfiguraciones=c.idconfiguraciones", array("idconfiguraciones"))
                ->join(array("t" => "templates"), "t.idtemplates=c.idtemplates", array("url", "numdiv"))
                ->where('idmonitores = ?', $id)
                ->setIntegrityCheck(false);
        return $this->fetchAll($select)->current();
    }
     public function getSingleRowbyIp($ip) {
        $select = $this->select()->from(array("m" => "monitores"), array("idmonitores", "ip", "refrescar","fecharegistro"))
                ->where('ip = ?', $ip)
                ->setIntegrityCheck(false);
        return $this->fetchAll($select)->current();
    }

    public function getSingleRowById($id) {
        $select = $this->select()->from(array("m" => "monitores"), array("idmonitores", "ip", "refrescar","fecharegistro"))
                ->where('idmonitores = ?', $id)
                ->setIntegrityCheck(false);
        return $this->fetchAll($select)->current();
    }

}
