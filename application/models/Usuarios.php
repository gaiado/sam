<?php

class Application_Model_Usuarios extends Zend_Db_Table_Abstract {

    protected $_name = 'usuarios';
    protected $_primary = 'idusuarios';

    public function getAll($id) {
        $select = $this->select()->from(array("u" => "usuarios"), array("*"))
                ->join(array("i" => "instituciones"), "i.idinstituciones = u.idinstituciones", array("nombrei" => "nombre"))
                ->setIntegrityCheck(false);
        return $this->fetchAll($select);
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function save($data, $id = null) {
        $data['password'] = sha1($data['password']);
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $row->setFromArray($data);
        $row->save();
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
    }

    public function getUser($login) {
        $select = $this->select()->where('usuario = ?', $login['usuario'])
                ->where('password = ?', sha1($login['password']))
                ->where('estado = ?', 1)
                ->limit(1);
        return $this->fetchAll($select)->current();
    }

}