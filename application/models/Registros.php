<?php

class Application_Model_Registros extends Zend_Db_Table_Abstract {

    protected $_name = 'registros';
    protected $_primary = 'idregistros';

    public function getAll() {
        $select = $this->select()->from(array("u" => "usuarios"), array("*"))
                ->join(array("i" => "instituciones"), "i.idinstituciones = u.idinstituciones", array("nombrei" => "nombre"))
                ->setIntegrityCheck(false);
        return $this->fetchAll($select);
    }

    public function save($data) {
        $session = new Zend_Session_Namespace('session');
        $row = $this->createRow();
        $row->setFromArray(array("ip"=> $session->ipusuario,"idusuarios"=>$session->usuario->idusuarios));
        $row->setFromArray($data);
        return $row->save();
    }
}
