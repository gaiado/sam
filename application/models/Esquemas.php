<?php

class Application_Model_Esquemas extends Zend_Db_Table_Abstract {

    protected $_name = 'esquemas';
    protected $_primary = 'idesquemas';

    public function getAll() {
        $select = $this->select()->from(array("e" => "esquemas"), array("*"))
                ->join(array("i" => "instituciones"), "i.idinstituciones = e.idinstituciones", array("nombrei" => "nombre"))
                ->join(array("c" => "configuraciones"), "c.idconfiguraciones=e.idconfiguraciones", array("idtem" => "idtemplates"))
                ->join(array("t" => "templates"), "t.idtemplates=c.idtemplates", array("url" => "url"))
                ->setIntegrityCheck(false);
        return $this->fetchAll($select);
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
        $row->setFromArray($data);
        $row->save();
    }

    public function getRowwithTemplate($id) {
        $select = $this->select()->from(array("e" => "esquemas"), array("*"))
                ->join(array("c" => "configuraciones"), "c.idconfiguraciones=e.idconfiguraciones", array("idtemplates" => "idtemplates"))
                ->join(array("t" => "templates"), "t.idtemplates=c.idtemplates", array("url" => "url", "estado" => "estado", "numdiv" => "numdiv", "idtemplates" => "idtemplates"))
                ->where('e.idesquemas = ?', $id)
                ->setIntegrityCheck(false);
        return $this->fetchAll($select)->current();
    }

    public function getKeyAsValue() {
        $datos = $this->getAll();
        $row = array();
        foreach ($datos as $value) {
            $row[$value->idesquemas] = $value->titulo;
        }
        return $row;
    }

}

