<?php

class Application_Model_Templates extends Zend_Db_Table_Abstract {

    protected $_name = 'templates';
    protected $_primary = 'idtemplates';

    public function getAll() {
        return $this->fetchAll();
    }

    public function getRow($id) {
        return $this->find($id)->current();
    }

    public function getAsKeyValue() {
        $rows = $this->fetchAll();
        foreach ($rows as $value) {
            $data[$value->idtemplates] = $value->titulo;
        }
        return $data;
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
        $row->setFromArray($data);
        $row->save();
        
    }

}