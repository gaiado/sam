<?php

class Application_Model_Instituciones extends Zend_Db_Table_Abstract {

    protected $_name = 'instituciones';
    protected $_primary = 'idinstituciones';

    public function getAll() {
        return $this->fetchAll();
    }

    public function getAsKeyValue() {
        $session = new Zend_Session_Namespace('session');
        $id = $session->institucion;
        $rows = $this->fetchAll();
        if ($id < 2) {
            foreach ($rows as $value) {
                $data[$value->idinstituciones] = $value->nombre;
            }
        } else {
            foreach ($rows as $value) {
                if ($id == $value->idinstituciones)
                    $data[$value->idinstituciones] = $value->nombre;
            }
        }
        return $data;
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
        $row->setFromArray($data);
        $row->save();        
    }

}