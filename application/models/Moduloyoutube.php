<?php

class Application_Model_Moduloyoutube extends Zend_Db_Table_Abstract {

    protected $_name = 'moduloyoutube';
    protected $_primary = 'idmoduloyoutube';

    public function getAll() {
        return $this->fetchAll();
    }

    public function save($data, $id = null) {
        if (is_null($id)) {
            $row = $this->createRow();
        } else {
            $row = $this->getRow($id);
        }
        $registro = new Application_Model_Registros();
        if (is_null($id)) {
            $registro->save(array("accion" => "insert", "tabla" => $this->_name, "id" => $this->getAdapter()->lastInsertId()));
        } else {
            $registro->save(array("accion" => "update", "tabla" => $this->_name, "id" => $id));
        }
        $row->setFromArray($data);
        return $row->save();
    }

    public function getRow($id) {
        $id = (int) $id;
        $row = $this->find($id)->current();
        return $row;
    }

}