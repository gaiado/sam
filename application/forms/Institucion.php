<?php

class Application_Form_Institucion extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'nombre', array(
            'label' => 'Nombre:',
            'required' => true
                )
        );

        $this->addElement(
                'text', 'direccion', array(
            'label' => 'Direccion:',
            'required' => true
                )
        );
        $this->addElement(
                'text', 'telefono', array(
            'label' => 'Telefono:',
            'required' => true
                )
        );
        $this->addElement(
                'text', 'logo', array(
            'label' => 'Logotipo:',
            'require' => true
            //, 'readonly' => true
                )
        );
        $this->addElement(
                'select', 'estilo', array(
            'label' => 'Estilo:',
            'required' => true
                )
        );
        $this->estilo->addMultiOptions(
                array("defaul" => "default")
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}