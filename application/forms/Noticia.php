<?php

class Application_Form_Noticia extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'titulo', array(
            'label' => 'Titulo:',
            'required' => true
                )
        );
        $this->addElement(
                'text', 'fechainicio', array(
            'label' => 'Fecha:'
                )
        );
        $this->addElement(
                'textarea', 'texto', array(
            'label' => 'Contenido:',
            'required' => true
                )
        );
        $this->addElement(
                'hidden', 'idmodulonoticias', array()
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}