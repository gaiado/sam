<?php

class Application_Form_Youtube extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'url', array(
            'label' => 'Url:',
            'required' => true
                )
        );
        $this->addElement(
                'hidden', 'idmoduloyoutubes', array()
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}
