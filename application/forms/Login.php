<?php

class Application_Form_Login extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'usuario', array(
            'label' => 'Usuario:',
            'required' => true
                )
        );

        $this->addElement(
                'password', 'password', array(
            'label' => 'Contraseña:',
            'required' => true
                )
        );

        $this->addElement(
                'submit', 'Ingresar', array()
        );
    }

}