<?php

class Application_Form_Video extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'titulo', array(
            'label' => 'Titulo:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'aleatorio', array(
            'label' => 'Aleatorio:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'repetir', array(
            'label' => 'Repetir:'
                )
        );
        $x = array('Inactivo', 'Activo');
        $this->aleatorio->addMultiOptions(
                $x
        );
        $this->repetir->addMultiOptions(
                $x
        );  

        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}