<?php

class Application_Form_Plantilla extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'titulo', array(
            'label' => 'Titulo:',
            'required' => true
                )
        );

        $this->addElement(
                'text', 'url', array(
            'label' => 'URL:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'numdiv', array(
            'label' => 'Numero de div\'s:'
                )
        );

        $this->addElement(
                'select', 'estado', array(
            'label' => 'Estado:',
            'required' => true
                )
        );
        $this->estado->addMultiOptions(
                array('1' => 'Activo', '0' => 'Inactivo')
        );
            
        $x=array();
        for ($index = 1; $index <=10; $index++) {
            $x[$index]=$index;
        }
        
        $this->numdiv->addMultiOptions(
                $x
        );


        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}
