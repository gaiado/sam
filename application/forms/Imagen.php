<?php

class Application_Form_Imagen extends Zend_Form {

    public function init() {
        $this->setAttrib('id', 'formulario');
        $this->setName('formulario');
        $this->addElement(
                'text', 'titulo', array(
            'label' => 'Titulo:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'repetir', array(
            'label' => 'Repetir:'
                )
        );
        $this->addElement(
                'select', 'aleatorio', array(
            'label' => 'Aleatorio:'
                )
        );
        $this->addElement(
                'select', 'efecto', array(
            'label' => 'Efecto:'
                )
        );
        $this->addElement(
                'select', 'tiempotransicion', array(
            'label' => 'Tiempo transicion:'
                )
        );
//        $this->addElement(
//                'file', 'imagen', array(
//            'accept' => 'image/*'
//                )
//        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
        $x = array('Inactivo', 'Activo');
        $this->aleatorio->addMultiOptions(
                $x
        );
        $this->repetir->addMultiOptions(
                $x
        );
        $x = array(
            1 => "blind",
            2 => "bounce",
            3 => "clip",
            4 => "drop",
            5 => "explode",
            6 => "fade",
            7 => "fold",
            8 => "highlight",
            9 => "puff",
            10 => "pulsate",
            11 => "shake",
            12 => "slide"
        );
        $this->efecto->addMultiOptions(
                $x
        );
        $x = array("10" => "10 seg", "30" => "30 seg", "60" => "1 min", "180" => "3 min", "300" => "5 min", "600" => "10 min", "1800" => "30 min", "3600" => "1 hor");
        $this->tiempotransicion->addMultiOptions(
                $x
        );
    }

}