<?php

class Application_Form_Usuario extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'nombre', array(
            'label' => 'Nombre:',
            'required' => true
                )
        );

        $this->addElement(
                'text', 'email', array(
            'label' => 'Email:',
            'required' => true
                )
        );
        $this->addElement(
                'text', 'usuario', array(
            'label' => 'Usuario:',
            'required' => true
                )
        );

        $this->addElement(
                'password', 'password', array(
            'label' => 'Contraseña:',
            'required' => true,
            'renderPassword' => true
                )
        );

        $this->addElement(
                'select', 'nivel', array(
            'label' => 'Nivel:'
                )
        );
        $session = new Zend_Session_Namespace('session');
        $id = $session->institucion;
        $user = $session->usuario;
        if ($id < 2) {
            $this->nivel->addMultiOptions(
                    array("Administrador", "Usuario A", "Usuario B")
            );
        } else {
            if ($user->nivel == 0)
                $this->nivel->addMultiOptions(
                        array("1" => "Usuario A", "2" => "Usuario B")
                );
            else
                $this->nivel->addMultiOptions(
                        array("2" => "Usuario B")
                );
        }

        $this->addElement(
                'select', 'idinstituciones', array(
            'label' => 'Institucion:'
                )
        );
        $model = new Application_Model_Instituciones();
        $this->idinstituciones->addMultiOptions(
                $model->getAsKeyValue()
        );
        $this->addElement(
                'select', 'estado', array(
            'label' => 'Estado:'
                )
        );
        $this->estado->addMultiOptions(
                array('1' => 'Activo', '0' => 'Inactivo')
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}