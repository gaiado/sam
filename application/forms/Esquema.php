<?php

class Application_Form_Esquema extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'titulo', array(
            'label' => 'Titulo:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'estado', array(
            'label' => 'Estado:',
            'required' => true
                )
        );
        $this->estado->addMultiOptions(
                array('1' => 'Activo', '0' => 'Inactivo')
        );
        $this->addElement(
                'select', 'idtemplates', array(
            'label' => 'Plantilla:'
                )
        );
        $model = new Application_Model_Templates();
        $rows = $model->getAsKeyValue();
        $this->idtemplates->addMultiOptions(
                $rows
        );
        $this->addElement(
                'select', 'idinstituciones', array(
            'label' => 'Institucion:',
            'required' => true
                )
        );
        $model = new Application_Model_Instituciones();
        $rows = $model->getAsKeyValue();
        unset($rows[1]);
        $this->idinstituciones->addMultiOptions(
                $rows
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}