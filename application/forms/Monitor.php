<?php

class Application_Form_Monitor extends Zend_Form {

    public function init() {
        $this->addElement(
                'text', 'ip', array(
            'label' => 'Ip:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'estado', array(
            'label' => 'Estado:',
            'required' => true
                )
        );

        $this->addElement(
                'select', 'idinstituciones', array(
            'label' => 'Institucion:'
                )
        );
        $model = new Application_Model_Instituciones();
        $rows = $model->getAsKeyValue();
        unset($rows[1]);
        $this->idinstituciones->addMultiOptions(
                $rows
        );

        $this->addElement(
                'text', 'marca', array(
            'label' => 'Marca:'
                )
        );
        $this->addElement(
                'text', 'modelo', array(
            'label' => 'Modelo:'
                )
        );

        $this->addElement(
                'select', 'idesquemas', array(
            'label' => 'Esquema:'
                )
        );

        $model = new Application_Model_Esquemas();
        $rows = $model->getKeyAsValue();
        $this->idesquemas->addMultiOptions(
                $rows
        );

        $this->estado->addMultiOptions(
                array("1" => "Activo", "0" => "Inactivo")
        );
        $this->addElement(
                'submit', 'Guardar', array()
        );
    }

}