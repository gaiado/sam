<?php

class EsquemaController extends Zend_Controller_Action
{

    public function init()
    {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function agregarAction()
    {
        $form = new Application_Form_Esquema();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Configuraciones();
                $idconfiguraciones = $model->save($form->getValues());
                $model = new Application_Model_Esquemas();
                $datos = $form->getValues();
                $datos['idconfiguraciones'] = $idconfiguraciones;
                $model->save($datos);
                return $this->_redirect("/esquema/configurar/idconfiguraciones/" . $idconfiguraciones);
            }
        }
        $this->view->form = $form;
    }

    public function listarAction()
    {
        $model = new Application_Model_Esquemas();
        $esquemas = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($esquemas);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->esquemas = $paginator;
    }

    public function configurarAction()
    {
        if ($this->_hasParam('idconfiguraciones')) {
            $model = new Application_Model_Configuraciones();
            $idconf = $this->_getParam('idconfiguraciones');
            $configuracion = $model->getRow($idconf);
            if (!isset($configuracion)) {
                return $this->_redirect("/esquema/listar");
            }
            $this->view->configuracion = $configuracion;
            $model = new Application_Model_Divs();
            $divs = $model->getByConfiguracion($idconf);
            $this->view->divs = $divs;

            $model = new Application_Model_Modulovideos();
            $videos = $model->getAll();
            $this->view->videos = $videos;

            $model = new Application_Model_Moduloimagenes();
            $imagenes = $model->getAll();
            $this->view->imagenes = $imagenes;

            $model = new Application_Model_Modulonoticias();
            $noticias = $model->getAll();
            $this->view->noticias = $noticias;
            
            $model = new Application_Model_Moduloyoutube;
            $youtubes = $model->getAll();
            $this->view->youtubes = $youtubes;
        } else {
            return $this->_redirect("/esquema/listar");
        }
    }

    public function updateAction()
    {
        if (!$this->_hasParam('esquema')) {
            return $this->_redirect('/esquema/listar/');
        }
        $esquema = $this->_getParam('esquema');
        $form = new Application_Form_Esquema();
        $form->getElement('idtemplates')->setAttrib('disabled', true);
        $model = new Application_Model_Esquemas();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model->save($form->getValues(), $esquema);
                $idcon = $model->getRow($esquema)->idconfiguraciones;
                return $this->_redirect('/esquema/configurar/idconfiguraciones/' . $idcon);
            }
        } else {
            $row = $model->getRowwithTemplate($this->_getParam('esquema'));
            if ($row) {
                $form->populate(array("titulo" => $row['titulo'], "estado" => $row['estado'], "idinstituciones" => $row['idinstituciones'], "idtemplates" => $row['idtemplates']));
            }
        }
        $this->view->form = $form;
    }

    public function deleteAction()
    {
        if (!$this->_hasParam('idesquemas')) {
            return $this->_redirect('/esquema/listar/');
        }
        $model = new Application_Model_Esquemas();
        $row = $model->getRow($this->_getParam('idesquemas'));
        if ($row)
            $row->delete();
        return $this->_redirect("/esquema/listar/");
        exit;
    }

    public function cargarAction()
    {
        $model = new Application_Model_Esquemas();
        $row = $model->getRowwithTemplate($this->_getParam('idesquemas'));
        if ($row)
            echo $row->url;
        exit;
    }

}
