<?php

class ImagenController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function agregarAction() {
        $form = new Application_Form_Imagen();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Moduloimagenes();
                $datos = $this->_getAllParams();
                $idmodulo = $model->save($datos);
//                $idmodulo = $model->getAdapter()->lastInsertId();
                $this->_redirect("/imagen/configurar/id/$idmodulo");
            }
        }
        $this->view->form = $form;
    }

    public function updateAction() {
        if (!$this->_hasParam('id')) {
            return $this->_redirect('/imagen/listar/');
        }
        $form = new Application_Form_Imagen();
        $model = new Application_Model_Moduloimagenes();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model->save($form->getValues(), $this->_getParam('id'));
                $this->_redirect("/imagen/configurar/id/" . $this->_getParam('id'));
            }
        } else {
            $row = $model->getRow($this->_getParam('id'));
            if ($row) {
                $form->populate(array("titulo" => $row['titulo'], "efecto" => $row['efecto'], "repetir" => $row['repetir'], "tiempotransicion" => $row['tiempotransicion'], "aleatorio" => $row['aleatorio']));
            }
        }
        $this->view->form = $form;
    }

    public function agregarurlAction() {
        if ($this->_hasParam('id')) {
            $idmodulo = $this->_getParam('id');
            $imagen = $this->_getParam('imagen');
            $model = new Application_Model_Imagenes();
            $data = array('url' => '/uploads/imagenes/' . $imagen, 'idmoduloimagenes' => $idmodulo, 'titulo' => $imagen);
            $model->save($data);
            $this->_redirect("/imagen/configurar/id/$idmodulo");
        }
        exit;
    }

    public function configurarAction() {
        if ($this->_hasParam('id')) {
            $model = new Application_Model_Imagenes();
            $imagenes = $model->getRowByModulo($this->_getParam('id'));
            $this->view->imagenes = $imagenes;
            $model = new Application_Model_Imagenes();
            $disponibles = $model->archivos();
            $this->view->dispo = $disponibles;
            $this->view->id = $this->_getParam('id');
        } else {
            $this->_redirect("/imagen/listar");
        }
    }

    public function listarAction() {
        $model = new Application_Model_Moduloimagenes();
        $modulos = $model->getAll();
        $this->view->paginator = $modulos;
    }

    public function eliminarlistaAction() {
        if ($this->_hasParam('idlista')) {
            $model = new Application_Model_Moduloimagenes();
            $datos = $model->getRow($this->_getParam('idlista'));
            $datos->delete();
        }
        $this->_redirect("/imagen/listar");
        exit;
    }

    public function eliminarurlAction() {
        if ($this->_hasParam('id')) {
            $model = new Application_Model_Imagenes();
            $datos = $model->getRow($this->_getParam('id'));
            $datos->delete();
            echo "OK";
        }
        exit;
    }

}
