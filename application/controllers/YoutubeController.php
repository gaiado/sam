<?php

class YoutubeController extends Zend_Controller_Action
{

    public function init()
    {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function listarAction()
    {
        $model = new Application_Model_Moduloyoutube;
        $listas = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($listas);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->listas = $paginator;
        
    }

    public function agregarAction()
    {
        $form = new Application_Form_Listayoutube();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Moduloyoutube();
                $id = $model->save($this->_getAllParams());
                return $this->_redirect("/youtube/update/idmodulo/" . $id);
            }
        }
        $this->view->form = $form;
    }

    public function updateAction()
    {
        if ($this->_hasParam("idmodulo")) {
            $idmodulo = $this->_getParam("idmodulo");
            $form = new Application_Form_Youtube();
            $model = new Application_Model_Youtube();           
            if ($this->getRequest()->isPost()) {
                
                if ($form->isValid($this->_getAllParams())) {
                    
                    $datos = $form->getValues();
                    $model->save($datos);
                    
                    $form->populate(array("url" => ""));
                }
            } 
            $youtubes = $model->getRowByModulo($idmodulo);
            $form->populate(array('idmoduloyoutubes' => $idmodulo));
            $this->view->form = $form;
            $this->view->noticias = $youtubes;
        } else {
            return $this->_redirect("/youtube/listar/");
        }
    }

    public function eliminaryoutubeAction()
    {
        if ($this->_hasParam("idnoticia")) {
            $model = new Application_Model_Youtube();
            $row = $model->getRow($this->_getParam('idnoticia'));
            $row->delete();
            echo "OK";
        }
        exit;
    }

    public function eliminarlistaAction()
    {
        if ($this->_hasParam("idlista")) {
            $model = new Application_Model_Moduloyoutube;
            $row = $model->getRow($this->_getParam('idlista'));
            $row->delete();
        }
        $this->_redirect("/youtube/listar");
        exit;
    }

}
