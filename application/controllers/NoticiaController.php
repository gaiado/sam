<?php

class NoticiaController extends Zend_Controller_Action
{

    public function init()
    {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function listarAction()
    {
        $model = new Application_Model_Modulonoticias();
        $listas = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($listas);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->listas = $paginator;
    }

    public function agregarAction()
    {
        $form = new Application_Form_Listanoticias();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Modulonoticias();
                $id = $model->save($this->_getAllParams());
//                $id = $model->getAdapter()->lastInsertId();
                return $this->_redirect("/noticia/update/idmodulo/" . $id);
            }
        }
        $this->view->form = $form;
    }

    public function listarNoticiasAction()
    {
        
    }

    public function updateAction()
    {

        if ($this->_hasParam("idmodulo")) {
            $idmodulo = $this->_getParam("idmodulo");
            $form = new Application_Form_Noticia();
            $model = new Application_Model_Noticias();
            if ($this->getRequest()->isPost()) {
                if ($form->isValid($this->_getAllParams())) {
                    $fecha = $this->_getParam('fechainicio');
                    $fecha = explode("/", $fecha);
                    $fecha = $fecha[2] . "/" . $fecha[0] . "/" . $fecha[1];
                    $datos = $form->getValues();
                    $datos['fechainicio'] = $fecha;
                    $model->save($datos);
                    $form->populate(array("titulo" => "", "fechainicio" => "", "texto" => ""));
                }
            } //else {
//                $row = $model->getRow($this->_getParam('idinstituciones'));
//                if ($row) {
//                    $form->populate(array("nombre" => $row['nombre'], "direccion" => $row['direccion'], "telefono" => $row['telefono'], "logo" => $row['logo'], "estilo" => $row['estilo']));
//                }
            //}
            $noticias = $model->getRowByModulo($idmodulo);
            $form->populate(array('idmodulonoticias' => $idmodulo));
            $this->view->form = $form;
            $this->view->noticias = $noticias;
        } else {
            return $this->_redirect("/noticia/listar/");
        }
    }

    public function eliminarnoticiaAction()
    {
        if ($this->_hasParam("idnoticia")) {
            $model = new Application_Model_Noticias();
            $row = $model->getRow($this->_getParam('idnoticia'));
            $row->delete();
            echo "OK";
        }
        exit;
    }

    public function eliminarlistaAction()
    {
        if ($this->_hasParam("idlista")) {
            $model = new Application_Model_Modulonoticias();
            $row = $model->getRow($this->_getParam('idlista'));
            $row->delete();
        }
        $this->_redirect("/noticia/listar");
        exit;
    }

}
