<?php

class UsuarioController extends Zend_Controller_Action {

    public function init() {
        
    }

    public function indexAction() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        } else {
            if ($session->usuario->nivel == "2")
                return $this->_redirect("/usuario/login");
            else
                return $this->_redirect("/usuario/listar");
        }
    }

    public function loginAction() {
        $ip = $this->getRequest()->getClientIp();
        $session = new Zend_Session_Namespace('session');
        Zend_Session::namespaceUnset('session');
        $form = new Application_Form_Login();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Usuarios();
                $row = $model->getUser($this->_getAllParams());
                if ($row) {
                    $row['fechaacceso'] = date("Y-m-d h:i:s");
                    $row->save();
                    $session->usuario = $row;
                    $session->institucion = $row['idinstituciones'];
                    $session->ipusuario = $ip;
                    return $this->_redirect('/');
                }
            }
        }
        $this->view->form = $form;
    }

    public function listarAction() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
        $model = new Application_Model_Usuarios();
        $usuarios = $model->getAll($session->usuario['idusuarios']);
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($usuarios);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->usuarios = $paginator;
    }

    public function deleteAction() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
        if (!$this->_hasParam('idusuarios')) {
            return $this->_redirect('/usuario/listar/');
        }
        $model = new Application_Model_Usuarios();
        $row = $model->getRow($this->_getParam('idusuarios'));
        if ($row)
            $row->delete();
        return $this->_redirect("/usuario/listar/");
        exit;
    }

    public function agregarAction() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
        $form = new Application_Form_Usuario();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Usuarios();
                $model->save($form->getValues());
                return $this->_redirect("/usuario/listar/");
            }
        }
        $this->view->form = $form;
    }

    public function updateAction() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
        if (!$this->_hasParam('idusuarios')) {
            return $this->_redirect('/usuario/listar/');
        }
        $form = new Application_Form_Usuario();
        $model = new Application_Model_Usuarios();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model->save($form->getValues(), $this->_getParam('idusuarios'));
                return $this->_redirect('/usuario/listar/');
            }
        } else {
            $row = $model->getRow($this->_getParam('idusuarios'));
            if ($row) {
                $form->populate(array("estado" => $row['estado'], "nombre" => $row['nombre'], "idinstituciones" => $row['idinstituciones'], "email" => $row['email'], "usuario" => $row['usuario'], 'nivel' => $row['nivel'], "password" => $row['password']));
            }
        }
        $this->view->form = $form;
    }

    public function logoutAction() {
        $session = new Zend_Session_Namespace('session');
        Zend_Session::namespaceUnset('session');
        return $this->_redirect("/");
        exit;
    }

}