<?php

class PantallaController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->disableLayout();
    }

    public function indexAction() {
        $model = new Application_Model_Monitores();
        if ($this->_hasParam('id')) {
            $monitor = $model->getRowById($this->_getParam('id'));
        } else {
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $monitor = $model->getRowbyIp($ip);
        }
        if (!isset($monitor)) {
            echo "Monitor no configurado<br>IP: $ip";
            exit;
        } else {
            $fecha = new Zend_Db_Expr('CURRENT_TIMESTAMP');
            $model->saveAuthLess(array('fecharegistro' => $fecha, 'ultimavista' => $fecha), $monitor->idmonitores); //actualizar fecha de ultima visualizacion
            $this->view->monitor = $monitor;
            $model = new Application_Model_Divs();
            $divs = $model->getByConfiguracion($monitor->idconfiguraciones);
            $this->view->divs = $divs;
        }
    }

    public function reloadAction() {
        $ip = $this->getRequest()->getServer('REMOTE_ADDR');
        $model = new Application_Model_Monitores();
        if ($this->_hasParam('id')) {
            $xx = $model->getSingleRowById($this->_getParam('id'));
        } else {
            $ip = $this->getRequest()->getServer('REMOTE_ADDR');
            $xx = $model->getSingleRowbyIp($ip);
        }
        
        $fecha = new Zend_Db_Expr('CURRENT_TIMESTAMP');
        $model->saveAuthLess(array('fecharegistro' => $fecha, 'ultimavista' => $fecha), $xx->idmonitores); //actualizar fecha de ultima visualizacion
        
        if (strtotime($xx->fecharegistro) < strtotime($xx->refrescar)) {
            echo "ok";
        } else {
            echo "x";
        }
        exit;
    }

}
