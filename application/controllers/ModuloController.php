<?php

class ModuloController extends Zend_Controller_Action {

    public function init() {
        $this->_helper->layout->disableLayout();
    }

    public function indexAction() {
        $modulo = $this->_getParam("modulo");
        $idmodulo = $this->_getParam("idmodulo");
        switch ($modulo) {
            case "1": $this->_redirect("/modulo/video/?idmodulo=" . $idmodulo);
                break;
            case "2":$this->_redirect("/modulo/imagen/?idmodulo=" . $idmodulo);
                break;
            case "3":$this->_redirect("/modulo/noticia/?idmodulo=" . $idmodulo);
                break;
            case "4":$this->_redirect("/modulo/youtube/?idmodulo=" . $idmodulo);
                break;
            default :echo "No asignado";
        }
        exit;
    }

    public function videoAction() {
        $idmodulo = $this->_getParam("idmodulo");
        $model = new Application_Model_Modulovideos();
        $configuracion = $model->getRow($idmodulo);
        $this->view->configuracion = $configuracion;
        $model = new Application_Model_Videos();
        $videos = $model->getRowByModulo($idmodulo);
        $lista = array();
        foreach ($videos as $video) {
            $lista[] = $video->url;
        }
        if ($configuracion->aleatorio) {
            shuffle($lista);
        }
        $idplayer = rand();
        $this->view->idplayer = $idplayer;
        $this->view->videos = $lista;
    }

    public function imagenAction() {
        $idmodulo = $this->_getParam("idmodulo");
        $model = new Application_Model_Moduloimagenes();
        $configuracion = $model->getRow($idmodulo);
        $this->view->configuracion = $configuracion;
        $model = new Application_Model_Imagenes();
        $imagenes = $model->getRowByModulo($idmodulo);
        $this->view->imagenes = $imagenes;
        $lista = array();
        foreach ($imagenes as $imagen) {
            $lista[] = $imagen->url;
        }
        if ($configuracion->aleatorio) {
            shuffle($lista);
        }
        $idplayer = rand();
        $this->view->imagenes = $lista;
        $this->view->idplayer = $idplayer;
    }

    public function noticiaAction() {
        $idmodulo = $this->_getParam("idmodulo");
        $model = new Application_Model_Modulonoticias();
        $configuracion = $model->getRow($idmodulo);
        $this->view->configuracion = $configuracion;
        $model = new Application_Model_Noticias();
        $noticias = $model->getRowByModulo($idmodulo);
        $this->view->noticias = $noticias;
        $lista = substr($lista, 0, -1);
        $this->view->lista = $lista;
        $idplayer = rand();
        $this->view->idplayer = $idplayer;
    }

    public function youtubeAction() {
        $idmodulo = $this->_getParam("idmodulo");
        $model = new Application_Model_Moduloyoutube;
        $configuracion = $model->getRow($idmodulo);
        $this->view->configuracion = $configuracion;
        $model = new Application_Model_Youtube();
        $videos = $model->getRowByModulo($idmodulo);

        $num = count($videos);
        $principal = false;
        $lista = '';
        if ($num == 1) {
            $principal = $videos->current()->url;
            $lista = '&playlist=' . $principal;
        } else if ($num > 1) {
            $l = [];
            $principal = $videos->current()->url;
            foreach ($videos as $video) {
                $l[] = $video->url;
            }
            //correcion por que youtube ahora pide toda la lista completa
            //unset($l[0]);
            $lista = '&playlist=' . implode(",", $l);
        }
        $this->view->principal = $principal;
        $this->view->lista = $lista;
    }

}
