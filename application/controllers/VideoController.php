<?php

class VideoController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function archivosAction() {        
        $carpeta = "uploads/videos";
        $ArrFicheros = scandir($carpeta);
        $archivos = "";
        foreach ($ArrFicheros as $value) {
            if ($value != "." && $value != "..")
                $archivos.=$value . "::";
        }
        echo $archivos;
        exit;
    }

    public function agregarAction() {
        $form = new Application_Form_Video();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $videos = $this->_getParam('videos');
                $model = new Application_Model_Modulovideos();
                $id=$model->save($form->getValues());              
                return $this->_redirect('/video/configurar/id/'.$id);
            }
        }
        $this->view->form = $form;
    }

    public function listarAction() {
        $model = new Application_Model_Modulovideos();
        $datos = $model->getAll();
        $this->view->modulovideos = $datos;
    }

    public function configurarAction() {
        if ($this->_hasParam('id')) {
            $model = new Application_Model_Videos();
            $videos = $model->getRowByModulo($this->_getParam('id'));
            $this->view->videos = $videos;
            $model = new Application_Model_Videos();
            $disponibles = $model->archivos();
            $this->view->dispo = $disponibles;
            $this->view->id = $this->_getParam('id');
        } else {
            $this->_redirect("/video/listar");
        }
    }

    public function cargarAction() {
        if ($this->_hasParam('idlista')) {
            $model = new Application_Model_Videos();
            $datos = $model->getRowByModulo($this->_getParam('idlista'));
            foreach ($datos as $video) {
                echo $video->url . '||' . $video->titulo . '::';
            }
        }
        exit;
    }

    public function eliminarlistaAction() {
        if ($this->_hasParam('idlista')) {
            $model = new Application_Model_Modulovideos();
            $datos = $model->getRow($this->_getParam('idlista'));
            $datos->delete();
        }
        $this->_redirect("/video/listar");
        exit;
    }
    public function eliminarurlAction() {
        if ($this->_hasParam('id')) {
            $model = new Application_Model_Videos();
            $datos = $model->getRow($this->_getParam('id'));
            $datos->delete();
            echo "OK";
        }
        exit;
    }
    public function agregarurlAction() {
        if ($this->_hasParam('id')) {
            $idmodulo = $this->_getParam('id');
            $imagen = $this->_getParam('video');
            $model = new Application_Model_Videos();
            $data = array('url' => '/uploads/videos/' . $imagen, 'idmodulovideos' => $idmodulo, 'titulo' => $imagen);
            $model->save($data);
            $this->_redirect("/video/configurar/id/$idmodulo");
        }
        exit;
    }

}