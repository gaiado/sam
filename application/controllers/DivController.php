<?php

class DivController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function actualizarAction() {
        $datos = explode("::", $this->_getParam('datos'));
        $iddivs = $datos[0];
        $tipomodulo = $datos[1];
        $idmodulo = $datos[2];

        if (!(is_null($idmodulo) && is_null($tipomodulo) && is_null($iddivs))) {
            $datos = array('tipomodulo' => $tipomodulo, 'idmodulo' => $idmodulo, 'estado' => '0');
            $model = new Application_Model_Divs();
            $model->save($datos, $iddivs);
            echo 'OK';
        }
        exit;
    }

    public function eliminarAction($iddivs) {
        $datos = array('tipomodulo' => $tipomodulo, 'idmodulo' => $idmodulo, 'estado' => '1');
        $model = new Application_Model_Divs();
        $model->save($datos, $iddivs);
        exit;
    }

    public function idAction() {
        $datos = explode("::", $this->_getParam('datos'));
        $model = new Application_Model_Divs();
        $div = $model->getByConfiguracionDiv($datos[1], $datos[0]);
        if (count($div))
            echo $div->idmodulo . "::" . $div->tipomodulo;
        exit;
    }

}
