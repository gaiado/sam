<?php

class TemplateController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function listarAction() {
        $model = new Application_Model_Templates();
        $templates = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($templates);
        $paginator->setItemCountPerPage(4);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->templates = $paginator;
    }

    public function agregarAction() {
        $form = new Application_Form_Plantilla();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Templates();
                $model->save($form->getValues());
                return $this->_redirect("/template/listar/");
            }
        }
        $this->view->form = $form;
    }

    public function cargarAction() {
        if ($this->_hasParam('id')) {
            $model = new Application_Model_Templates();
            $row = $model->getRow($this->_getParam('id'));
            echo $row->url;
        }
        exit;
    }

    public function updateAction() {
        if (!$this->_hasParam('id')) {
            return $this->_redirect('/template/listar/');
        }
        $form = new Application_Form_Plantilla();
        $model = new Application_Model_Templates();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model->save($form->getValues(), $this->_getParam('id'));
                return $this->_redirect('/template/listar/');
            }
        } else {
            $row = $model->getRow($this->_getParam('id'));
            if ($row) {
                $form->populate(array("titulo" => $row['titulo'], "url" => $row['url'], "estado" => $row['estado'],"numdiv"=>$row['numdiv']));
            }
        }
        $this->view->form = $form;
    }

}
