<?php

class MonitorController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function agregarAction() {
        $form = new Application_Form_Monitor();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Monitores();
                $model->save($form->getValues());
                return $this->_redirect("/monitor/listar/");
            }
        }
        $this->view->form = $form;
    }

    public function updateAction() {
        if (!$this->_hasParam('idmonitores')) {
            return $this->_redirect('/monitor/listar/');
        }
        $form = new Application_Form_Monitor();
        $model = new Application_Model_Monitores();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $fecha = new Zend_Db_Expr('CURRENT_TIMESTAMP');
                $model->save(array('refrescar' => $fecha, 'ip' => $this->_getParam('ip'),'marca'=>$this->_getParam('marca'),'modelo'=>$this->_getParam('modelo'),'idinstituciones'=>$this->_getParam('idinstituciones'),'idesquemas'=>$this->_getParam('idesquemas'),'estado'=>$this->_getParam('estado')), $this->_getParam('idmonitores'));
                return $this->_redirect('/monitor/listar/');
            }
        } else {
            $row = $model->getRow($this->_getParam('idmonitores'));
            if ($row) {
                $form->populate(array("ip" => $row->ip, "estado" => $row->estado, "idinstituciones" => $row->idinstituciones, "marca" => $row->marca, "modelo" => $row->modelo, "idesquemas" => $row->idesquemas));
            }
        }
        $this->view->form = $form;
    }

    public function deleteAction() {
        if (!$this->_hasParam('idmonitores')) {
            return $this->_redirect('/monitor/listar/');
        }
        $model = new Application_Model_Monitores();
        $row = $model->getRow($this->_getParam('idmonitores'));
        if ($row)
            $row->delete();
        return $this->_redirect("/monitor/listar/");
        exit;
    }

    public function listarAction() {
        $session = new Zend_Session_Namespace('session');
        $model = new Application_Model_Monitores();
        $monitores = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($monitores);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->monitores = $paginator;
        $this->view->idi = $session->institucion;
    }

    public function detallesAction() {
        if (!$this->_hasParam('idmonitores')) {
            echo "Selecione un monitor";
        }
        $session = new Zend_Session_Namespace('session');
        $model = new Application_Model_Monitores();
        $monitor = $model->find((int) $this->_getParam('idmonitores'));
        $model = new Application_Model_Instituciones();
        if (count($monitor)) {
            $monitor = $monitor[0];
            $institucion = $model->getRow($monitor->idinstituciones);
            ?>
            <div id="col1">
                <label>Marca :</label>
                <label>Modelo :</label>
                <label>IP :</label>
            </div>
            <div id="col2">
                <label><?php echo $monitor->marca; ?></label>
                <label><?php echo $monitor->modelo; ?></label>
                <label><?php echo $monitor->ip; ?></label>
            </div>
            <div id="col3">
                <label>Registrado:</label>
                <label>Estado :</label>
                <label>Institucion :</label></div>
            <div id="col4">
                <label><?php echo $monitor->fecharegistro; ?></label>
                <label style="color: <?php echo ($monitor->estado == 1) ? 'green' : 'red'; ?>" ><?php echo ($monitor->estado == 1) ? 'Activo' : 'Inactivo'; ?></label>
                <label><?php echo $institucion->nombre; ?></label></div>
            <div id="plantilla" style="height: 285px; margin-top: 70px;" class="well">
                <?php echo $monitor->idesquemas; ?>
            </div>
            <script type="text/javascript">
                var plantilla = $("#plantilla");
                $.post(baseUrl+'esquema/cargar/idesquemas/' + plantilla.text(), function(data) {
                    if (data != "")
                        plantilla.load(baseUrl+data + "index.php");
                });
            </script>
            <?php
        }
        exit;
    }

}