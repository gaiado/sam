<?php

class InstitucionController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function agregarAction() {
        $form = new Application_Form_Institucion();
        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model = new Application_Model_Instituciones();
                $model->save($form->getValues());
                return $this->_redirect("/institucion/listar/");
            }
        }
        $this->view->form = $form;
    }

    public function updateAction() {
        if (!$this->_hasParam('idinstituciones')) {
            return $this->_redirect('/institucion/listar/');
        }
        $form = new Application_Form_Institucion();
        $model = new Application_Model_Instituciones();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($this->_getAllParams())) {
                $model->save($form->getValues(), $this->_getParam('idinstituciones'));
                return $this->_redirect('/institucion/listar/');
            }
        } else {
            $row = $model->getRow($this->_getParam('idinstituciones'));
            if ($row) {
                $form->populate(array("nombre"=>$row['nombre'],"direccion"=>$row['direccion'],"telefono"=>$row['telefono'],"logo"=>$row['logo'],"estilo"=>$row['estilo']));
            }
        }
        $this->view->form = $form;
    }

    public function listarAction() {
        $model = new Application_Model_Instituciones();
        $instituciones = $model->getAll();
        Zend_View_Helper_PaginationControl::setDefaultViewPartial("/paginator/items.phtml");
        $paginator = Zend_Paginator::factory($instituciones);
        if ($this->_hasParam('page')) {
            $paginator->setCurrentPageNumber($this->_getParam('page'));
        }
        $this->view->instituciones = $paginator;
    }

    public function selecionarAction() {
        if ($this->_hasParam('id')) {
            $id = $this->_getParam('id');
            if (is_numeric($id)) {
                $session = new Zend_Session_Namespace('session');
                $session->institucion = $id;
            }
        }
        return $this->_redirect("/");
        exit;
    }

}