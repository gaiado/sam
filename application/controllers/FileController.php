<?php

class FileController extends Zend_Controller_Action {

    public function init() {
        $session = new Zend_Session_Namespace('session');
        if (!isset($session->usuario)) {
            return $this->_redirect("/usuario/login");
        }
    }

    public function indexAction() {
        
    }

}

