-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 28-07-2014 a las 13:26:47
-- Versión del servidor: 5.6.17
-- Versión de PHP: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

CREATE TABLE IF NOT EXISTS `configuraciones` (
  `idconfiguraciones` int(11) NOT NULL AUTO_INCREMENT,
  `idtemplates` int(11) NOT NULL,
  PRIMARY KEY (`idconfiguraciones`),
  KEY `fk_configuracionesquema_templates1` (`idtemplates`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`idconfiguraciones`, `idtemplates`) VALUES
(1, 1),
(2, 1);

--
-- Disparadores `configuraciones`
--
DROP TRIGGER IF EXISTS `div_avaliable`;
DELIMITER //
CREATE TRIGGER `div_avaliable` AFTER INSERT ON `configuraciones`
 FOR EACH ROW BEGIN
    DECLARE idcon INT;
    DEClARE ndiv INT;
    DECLARE i INT;
    SELECT MAX(idconfiguraciones) FROM configuraciones INTO idcon;
    SELECT numdiv FROM templates WHERE idtemplates=(SELECT idtemplates FROM configuraciones WHERE idconfiguraciones =idcon) INTO ndiv; 
    SET i=1;    
    WHILE(i<=ndiv) DO
        INSERT INTO divs VALUES(NULL,idcon,NULL,NULL,i,1,NOW());
        SET i=i+1;
    END WHILE;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divs`
--

CREATE TABLE IF NOT EXISTS `divs` (
  `iddivs` int(11) NOT NULL AUTO_INCREMENT,
  `idconfiguraciones` int(11) NOT NULL,
  `tipomodulo` int(11) DEFAULT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  `numdiv` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechamodificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddivs`),
  KEY `fk_divs_configuraciones1` (`idconfiguraciones`),
  KEY `fk_divs_modulos1` (`tipomodulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Disparadores `divs`
--
DROP TRIGGER IF EXISTS `updateDiv`;
DELIMITER //
CREATE TRIGGER `updateDiv` AFTER UPDATE ON `divs`
 FOR EACH ROW BEGIN
    DECLARE ides INT;    
    SELECT idesquemas FROM esquemas WHERE idconfiguraciones=OLD.idconfiguraciones INTO ides;
    UPDATE monitores SET refrescar = NOW() WHERE idesquemas=ides;      
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquemas`
--

CREATE TABLE IF NOT EXISTS `esquemas` (
  `idesquemas` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idconfiguraciones` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idesquemas`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_esquemas_instituciones1` (`idinstituciones`),
  KEY `fk_esquemas_configuraciones1` (`idconfiguraciones`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquemasusuarios`
--

CREATE TABLE IF NOT EXISTS `esquemasusuarios` (
  `idesquemas` int(11) NOT NULL,
  `idusuarios` int(11) NOT NULL,
  KEY `fk_esquemas_has_usuarios_usuarios1` (`idusuarios`),
  KEY `fk_esquemas_has_usuarios_esquemas1` (`idesquemas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

CREATE TABLE IF NOT EXISTS `imagenes` (
  `idimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `idmoduloimagenes` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idimagenes`),
  KEY `fk_listaimagenes_moduloimagenes1` (`idmoduloimagenes`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Disparadores `imagenes`
--
DROP TRIGGER IF EXISTS `deleteImagen`;
DELIMITER //
CREATE TRIGGER `deleteImagen` BEFORE DELETE ON `imagenes`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=OLD.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `insertImagen`;
DELIMITER //
CREATE TRIGGER `insertImagen` AFTER INSERT ON `imagenes`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=NEW.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;   
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instituciones`
--

CREATE TABLE IF NOT EXISTS `instituciones` (
  `idinstituciones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `estilo` varchar(45) DEFAULT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idinstituciones`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `instituciones`
--

INSERT INTO `instituciones` (`idinstituciones`, `nombre`, `direccion`, `telefono`, `logo`, `estilo`, `fecharegistro`) VALUES
(1, 'ninguna', '', '', '', NULL, '2013-04-30 13:55:21'),
(2, 'Universidad TecnolÃ³gica de Cancun', 'xxx', 'xxx', 'xxxx', 'defaul', '2013-04-30 13:55:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduloimagenes`
--

CREATE TABLE IF NOT EXISTS `moduloimagenes` (
  `idmoduloimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmoduloimagenes`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulonoticias`
--

CREATE TABLE IF NOT EXISTS `modulonoticias` (
  `idmodulonoticias` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmodulonoticias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

CREATE TABLE IF NOT EXISTS `modulos` (
  `idmodulos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `tabla` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmodulos`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`idmodulos`, `nombre`, `tabla`, `fecharegistro`) VALUES
(1, 'videos', 'modulovideos', '2013-04-30 14:11:54'),
(2, 'imagenes', 'moduloimagenes', '2013-04-30 14:11:54'),
(3, 'noticias', 'modulonoticias', '2013-04-30 14:12:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulovideos`
--

CREATE TABLE IF NOT EXISTS `modulovideos` (
  `idmodulovideos` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idmodulovideos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monitores`
--

CREATE TABLE IF NOT EXISTS `monitores` (
  `idmonitores` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idesquemas` int(11) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `refrescar` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmonitores`),
  KEY `fk_monitores_instituciones1` (`idinstituciones`),
  KEY `fk_monitores_esquemas1` (`idesquemas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `idnoticias` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulonoticias` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `texto` text NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idnoticias`),
  KEY `fk_noticias_modulonoticias1` (`idmodulonoticias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Disparadores `noticias`
--
DROP TRIGGER IF EXISTS `deleteNoticia`;
DELIMITER //
CREATE TRIGGER `deleteNoticia` BEFORE DELETE ON `noticias`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=OLD.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `insertNoticia`;
DELIMITER //
CREATE TRIGGER `insertNoticia` AFTER INSERT ON `noticias`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=NEW.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;   
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `idregistros` int(11) NOT NULL AUTO_INCREMENT,
  `idusuarios` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accion` varchar(45) NOT NULL,
  `tabla` varchar(45) NOT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idregistros`),
  KEY `fk_registros_usuarios1` (`idusuarios`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templates`
--

CREATE TABLE IF NOT EXISTS `templates` (
  `idtemplates` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `numdiv` int(11) NOT NULL,
  PRIMARY KEY (`idtemplates`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `templates`
--

INSERT INTO `templates` (`idtemplates`, `titulo`, `url`, `estado`, `numdiv`) VALUES
(1, 'plantilla1', '/uploads/plantillas/plantilla1/', 1, 4),
(2, 'plantilla2', '/uploads/plantillas/plantilla2/', 1, 5),
(3, 'plantilla3', '/uploads/plantillas/plantilla3/', 1, 5),
(4, 'plantilla4', '/uploads/plantillas/plantilla4/', 1, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nivel` tinyint(4) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaacceso` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idusuarios`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_usuarios_instituciones1` (`idinstituciones`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `idinstituciones`, `nombre`, `email`, `usuario`, `password`, `nivel`, `estado`, `fecharegistro`, `fechaacceso`) VALUES
(1, 1, 'administrador', '', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 0, 1, '2013-04-30 13:56:40', '2013-04-30 14:23:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `idvideos` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulovideos` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idvideos`),
  KEY `fk_listavideos_modulovideos1` (`idmodulovideos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Disparadores `videos`
--
DROP TRIGGER IF EXISTS `deleteVideo`;
DELIMITER //
CREATE TRIGGER `deleteVideo` BEFORE DELETE ON `videos`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmodulovideos FROM videos WHERE idvideos=OLD.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;
END
//
DELIMITER ;
DROP TRIGGER IF EXISTS `insertVideo`;
DELIMITER //
CREATE TRIGGER `insertVideo` AFTER INSERT ON `videos`
 FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmodulovideos FROM videos WHERE idvideos=NEW.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;   
END
//
DELIMITER ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD CONSTRAINT `fk_configuracionesquema_templates1` FOREIGN KEY (`idtemplates`) REFERENCES `templates` (`idtemplates`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `divs`
--
ALTER TABLE `divs`
  ADD CONSTRAINT `fk_divs_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_divs_modulos1` FOREIGN KEY (`tipomodulo`) REFERENCES `modulos` (`idmodulos`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Filtros para la tabla `esquemas`
--
ALTER TABLE `esquemas`
  ADD CONSTRAINT `fk_esquemas_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_esquemas_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `esquemasusuarios`
--
ALTER TABLE `esquemasusuarios`
  ADD CONSTRAINT `fk_esquemas_has_usuarios_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_esquemas_has_usuarios_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_listaimagenes_moduloimagenes1` FOREIGN KEY (`idmoduloimagenes`) REFERENCES `moduloimagenes` (`idmoduloimagenes`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `monitores`
--
ALTER TABLE `monitores`
  ADD CONSTRAINT `fk_monitores_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_monitores_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `fk_noticias_modulonoticias1` FOREIGN KEY (`idmodulonoticias`) REFERENCES `modulonoticias` (`idmodulonoticias`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `registros`
--
ALTER TABLE `registros`
  ADD CONSTRAINT `fk_registros_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk_listavideos_modulovideos1` FOREIGN KEY (`idmodulovideos`) REFERENCES `modulovideos` (`idmodulovideos`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
