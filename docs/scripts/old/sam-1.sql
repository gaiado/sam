ALTER TABLE `sam`.`monitores` CHANGE COLUMN `refrescar` `refrescar` INT DEFAULT NULL  ;

UPDATE monitores SET refrescar = NULL;

ALTER TABLE `sam`.`monitores` CHANGE COLUMN `refrescar` `refrescar` TIMESTAMP NULL DEFAULT NULL  ;

ALTER TABLE `sam`.`divs` CHANGE COLUMN `fechamodificacion` `fechamodificacion` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP  AFTER `estado` ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`div_avaliable` $$


DELIMITER ;


DELIMITER $$

USE `sam`$$


CREATE TRIGGER div_avaliable AFTER INSERT ON configuraciones
FOR EACH ROW
BEGIN
    DECLARE idcon INT;
    DEClARE ndiv INT;
    DECLARE i INT;
    SELECT MAX(idconfiguraciones) FROM configuraciones INTO idcon;
    SELECT numdiv FROM templates WHERE idtemplates=(SELECT idtemplates FROM configuraciones WHERE idconfiguraciones =idcon) INTO ndiv; 
    SET i=1;    
    WHILE(i<=ndiv) DO
        INSERT INTO divs VALUES(NULL,idcon,NULL,NULL,i,1,NULL);
        SET i=i+1;
    END WHILE;
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$

DROP TRIGGER IF EXISTS `sam`.`insertNoticia` $$


CREATE TRIGGER insertNoticia AFTER INSERT ON noticias
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=NEW.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;   
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`deleteNoticia` $$

CREATE TRIGGER deleteNoticia BEFORE DELETE ON noticias
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=OLD.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`insertVideo` $$

CREATE TRIGGER insertVideo AFTER INSERT ON videos
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmodulovideos FROM videos WHERE idvideos=NEW.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;   
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`deleteVideo` $$

CREATE TRIGGER deleteVideo BEFORE DELETE ON videos
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmodulovideos FROM videos WHERE idvideos=OLD.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`deleteImagen` $$

CREATE TRIGGER deleteImagen BEFORE DELETE ON imagenes
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=OLD.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`insertImagen` $$

CREATE TRIGGER insertImagen AFTER INSERT ON imagenes
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=NEW.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;   
END$$


DELIMITER ;


DELIMITER $$

USE `sam`$$
DROP TRIGGER IF EXISTS `sam`.`updateDiv` $$

CREATE TRIGGER updateDiv AFTER UPDATE ON divs
FOR EACH ROW
BEGIN
    DECLARE ides INT;    
    SELECT idesquemas FROM esquemas WHERE idconfiguraciones=OLD.idconfiguraciones INTO ides;
    UPDATE monitores SET refrescar = NOW() WHERE idesquemas=ides;      
END$$


DELIMITER ;
