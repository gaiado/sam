SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `sam`.`esquemas` DROP FOREIGN KEY `fk_esquemas_instituciones1` ;

ALTER TABLE `sam`.`esquemas` 
  ADD CONSTRAINT `fk_esquemas_instituciones1`
  FOREIGN KEY (`idinstituciones` )
  REFERENCES `sam`.`instituciones` (`idinstituciones` )
  ON DELETE CASCADE
  ON UPDATE NO ACTION;

ALTER TABLE `sam`.`monitores` DROP FOREIGN KEY `fk_monitores_instituciones1` ;

ALTER TABLE `sam`.`monitores` 
  ADD CONSTRAINT `fk_monitores_instituciones1`
  FOREIGN KEY (`idinstituciones` )
  REFERENCES `sam`.`instituciones` (`idinstituciones` )
  ON DELETE CASCADE
  ON UPDATE NO ACTION;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
