-- MySQL dump 10.14  Distrib 10.0.1-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: sam
-- ------------------------------------------------------
-- Server version	10.0.1-MariaDB-mariadb1~quantal-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `configuraciones`
--

DROP TABLE IF EXISTS `configuraciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `configuraciones` (
  `idconfiguraciones` int(11) NOT NULL AUTO_INCREMENT,
  `idtemplates` int(11) NOT NULL,
  PRIMARY KEY (`idconfiguraciones`),
  KEY `fk_configuracionesquema_templates1` (`idtemplates`),
  CONSTRAINT `fk_configuracionesquema_templates1` FOREIGN KEY (`idtemplates`) REFERENCES `templates` (`idtemplates`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `configuraciones`
--

LOCK TABLES `configuraciones` WRITE;
/*!40000 ALTER TABLE `configuraciones` DISABLE KEYS */;
INSERT INTO `configuraciones` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `configuraciones` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER div_avaliable AFTER INSERT ON configuraciones
FOR EACH ROW
BEGIN
    DECLARE idcon INT;
    DEClARE ndiv INT;
    DECLARE i INT;
    SELECT MAX(idconfiguraciones) FROM configuraciones INTO idcon;
    SELECT numdiv FROM templates WHERE idtemplates=(SELECT idtemplates FROM configuraciones WHERE idconfiguraciones =idcon) INTO ndiv; 
    SET i=1;    
    WHILE(i<=ndiv) DO
        INSERT INTO divs VALUES(NULL,idcon,NULL,NULL,i,1,NULL);
        SET i=i+1;
    END WHILE;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `divs`
--

DROP TABLE IF EXISTS `divs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `divs` (
  `iddivs` int(11) NOT NULL AUTO_INCREMENT,
  `idconfiguraciones` int(11) NOT NULL,
  `tipomodulo` int(11) DEFAULT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  `numdiv` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechamodificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddivs`),
  KEY `fk_divs_configuraciones1` (`idconfiguraciones`),
  KEY `fk_divs_modulos1` (`tipomodulo`),
  CONSTRAINT `fk_divs_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_divs_modulos1` FOREIGN KEY (`tipomodulo`) REFERENCES `modulos` (`idmodulos`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divs`
--

LOCK TABLES `divs` WRITE;
/*!40000 ALTER TABLE `divs` DISABLE KEYS */;
/*!40000 ALTER TABLE `divs` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER updateDiv AFTER UPDATE ON divs
FOR EACH ROW
BEGIN
    DECLARE ides INT;    
    SELECT idesquemas FROM esquemas WHERE idconfiguraciones=OLD.idconfiguraciones INTO ides;
    UPDATE monitores SET refrescar = NOW() WHERE idesquemas=ides;      
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `esquemas`
--

DROP TABLE IF EXISTS `esquemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esquemas` (
  `idesquemas` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idconfiguraciones` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idesquemas`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_esquemas_instituciones1` (`idinstituciones`),
  KEY `fk_esquemas_configuraciones1` (`idconfiguraciones`),
  CONSTRAINT `fk_esquemas_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_esquemas_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esquemas`
--

LOCK TABLES `esquemas` WRITE;
/*!40000 ALTER TABLE `esquemas` DISABLE KEYS */;
/*!40000 ALTER TABLE `esquemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `esquemasusuarios`
--

DROP TABLE IF EXISTS `esquemasusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `esquemasusuarios` (
  `idesquemas` int(11) NOT NULL,
  `idusuarios` int(11) NOT NULL,
  KEY `fk_esquemas_has_usuarios_usuarios1` (`idusuarios`),
  KEY `fk_esquemas_has_usuarios_esquemas1` (`idesquemas`),
  CONSTRAINT `fk_esquemas_has_usuarios_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_esquemas_has_usuarios_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `esquemasusuarios`
--

LOCK TABLES `esquemasusuarios` WRITE;
/*!40000 ALTER TABLE `esquemasusuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `esquemasusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `imagenes` (
  `idimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `idmoduloimagenes` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idimagenes`),
  KEY `fk_listaimagenes_moduloimagenes1` (`idmoduloimagenes`),
  CONSTRAINT `fk_listaimagenes_moduloimagenes1` FOREIGN KEY (`idmoduloimagenes`) REFERENCES `moduloimagenes` (`idmoduloimagenes`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `imagenes`
--

LOCK TABLES `imagenes` WRITE;
/*!40000 ALTER TABLE `imagenes` DISABLE KEYS */;
/*!40000 ALTER TABLE `imagenes` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insertImagen AFTER INSERT ON imagenes
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=NEW.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;   
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER deleteImagen BEFORE DELETE ON imagenes
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=OLD.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `instituciones`
--

DROP TABLE IF EXISTS `instituciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `instituciones` (
  `idinstituciones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `estilo` varchar(45) DEFAULT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idinstituciones`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `instituciones`
--

LOCK TABLES `instituciones` WRITE;
/*!40000 ALTER TABLE `instituciones` DISABLE KEYS */;
INSERT INTO `instituciones` VALUES (1,'ninguna','','','',NULL,'2013-04-30 13:55:21'),(2,'Universidad TecnolÃ³gica de Cancun','xxx','xxx','xxxx','defaul','2013-04-30 13:55:58');
/*!40000 ALTER TABLE `instituciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `moduloimagenes`
--

DROP TABLE IF EXISTS `moduloimagenes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `moduloimagenes` (
  `idmoduloimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmoduloimagenes`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `moduloimagenes`
--

LOCK TABLES `moduloimagenes` WRITE;
/*!40000 ALTER TABLE `moduloimagenes` DISABLE KEYS */;
/*!40000 ALTER TABLE `moduloimagenes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulonoticias`
--

DROP TABLE IF EXISTS `modulonoticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulonoticias` (
  `idmodulonoticias` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmodulonoticias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulonoticias`
--

LOCK TABLES `modulonoticias` WRITE;
/*!40000 ALTER TABLE `modulonoticias` DISABLE KEYS */;
/*!40000 ALTER TABLE `modulonoticias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulos`
--

DROP TABLE IF EXISTS `modulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulos` (
  `idmodulos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `tabla` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmodulos`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulos`
--

LOCK TABLES `modulos` WRITE;
/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
INSERT INTO `modulos` VALUES (1,'videos','modulovideos','2013-04-30 14:11:54'),(2,'imagenes','moduloimagenes','2013-04-30 14:11:54'),(3,'noticias','modulonoticias','2013-04-30 14:12:09');
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modulovideos`
--

DROP TABLE IF EXISTS `modulovideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modulovideos` (
  `idmodulovideos` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idmodulovideos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modulovideos`
--

LOCK TABLES `modulovideos` WRITE;
/*!40000 ALTER TABLE `modulovideos` DISABLE KEYS */;
/*!40000 ALTER TABLE `modulovideos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `monitores`
--

DROP TABLE IF EXISTS `monitores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `monitores` (
  `idmonitores` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idesquemas` int(11) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `refrescar` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmonitores`),
  KEY `fk_monitores_instituciones1` (`idinstituciones`),
  KEY `fk_monitores_esquemas1` (`idesquemas`),
  CONSTRAINT `fk_monitores_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_monitores_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `monitores`
--

LOCK TABLES `monitores` WRITE;
/*!40000 ALTER TABLE `monitores` DISABLE KEYS */;
/*!40000 ALTER TABLE `monitores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `noticias`
--

DROP TABLE IF EXISTS `noticias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noticias` (
  `idnoticias` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulonoticias` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `texto` text NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idnoticias`),
  KEY `fk_noticias_modulonoticias1` (`idmodulonoticias`),
  CONSTRAINT `fk_noticias_modulonoticias1` FOREIGN KEY (`idmodulonoticias`) REFERENCES `modulonoticias` (`idmodulonoticias`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `noticias`
--

LOCK TABLES `noticias` WRITE;
/*!40000 ALTER TABLE `noticias` DISABLE KEYS */;
/*!40000 ALTER TABLE `noticias` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insertNoticia AFTER INSERT ON noticias
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=NEW.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;   
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER deleteNoticia BEFORE DELETE ON noticias
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=OLD.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `templates`
--

DROP TABLE IF EXISTS `templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `templates` (
  `idtemplates` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `numdiv` int(11) NOT NULL,
  PRIMARY KEY (`idtemplates`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `templates`
--

LOCK TABLES `templates` WRITE;
/*!40000 ALTER TABLE `templates` DISABLE KEYS */;
INSERT INTO `templates` VALUES (1,'plantilla1','/uploads/plantillas/plantilla1/',1,4),(2,'plantilla2','/uploads/plantillas/plantilla2/',1,5),(3,'plantilla3','/uploads/plantillas/plantilla3/',1,5),(4,'plantilla4','/uploads/plantillas/plantilla4/',1,7),(5,'plantilla5','/uploads/plantillas/plantilla5/',1,3);
/*!40000 ALTER TABLE `templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nivel` tinyint(4) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaacceso` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idusuarios`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_usuarios_instituciones1` (`idinstituciones`),
  CONSTRAINT `fk_usuarios_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,1,'administrador','','admin',SHA('admin'),0,1,'2013-04-30 13:56:40','2013-04-30 14:23:00');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `idvideos` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulovideos` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idvideos`),
  KEY `fk_listavideos_modulovideos1` (`idmodulovideos`),
  CONSTRAINT `fk_listavideos_modulovideos1` FOREIGN KEY (`idmodulovideos`) REFERENCES `modulovideos` (`idmodulovideos`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insertVideo AFTER INSERT ON videos
FOR EACH ROW
BEGIN
    DECLARE idimg INT;   
    SELECT idmodulovideos FROM videos WHERE idvideos=NEW.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;   
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER deleteVideo BEFORE DELETE ON videos
FOR EACH ROW
BEGIN
    DECLARE idimg INT;    
    SELECT idmodulovideos FROM videos WHERE idvideos=OLD.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'sam'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-04-30  9:27:06
