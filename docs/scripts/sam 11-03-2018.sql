-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 12-03-2018 a las 01:46:58
-- Versión del servidor: 5.7.19
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "-05:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sam`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuraciones`
--

DROP TABLE IF EXISTS `configuraciones`;
CREATE TABLE IF NOT EXISTS `configuraciones` (
  `idconfiguraciones` int(11) NOT NULL AUTO_INCREMENT,
  `idtemplates` int(11) NOT NULL,
  PRIMARY KEY (`idconfiguraciones`),
  KEY `fk_configuracionesquema_templates1` (`idtemplates`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `configuraciones`
--

INSERT INTO `configuraciones` (`idconfiguraciones`, `idtemplates`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1);

--
-- Disparadores `configuraciones`
--
DROP TRIGGER IF EXISTS `div_avaliable`;
DELIMITER $$
CREATE TRIGGER `div_avaliable` AFTER INSERT ON `configuraciones` FOR EACH ROW BEGIN
    DECLARE idcon INT;
    DEClARE ndiv INT;
    DECLARE i INT;
    SELECT MAX(idconfiguraciones) FROM configuraciones INTO idcon;
    SELECT numdiv FROM templates WHERE idtemplates=(SELECT idtemplates FROM configuraciones WHERE idconfiguraciones =idcon) INTO ndiv; 
    SET i=1;    
    WHILE(i<=ndiv) DO
        INSERT INTO divs VALUES(NULL,idcon,NULL,NULL,i,1,NOW());
        SET i=i+1;
    END WHILE;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `divs`
--

DROP TABLE IF EXISTS `divs`;
CREATE TABLE IF NOT EXISTS `divs` (
  `iddivs` int(11) NOT NULL AUTO_INCREMENT,
  `idconfiguraciones` int(11) NOT NULL,
  `tipomodulo` int(11) DEFAULT NULL,
  `idmodulo` int(11) DEFAULT NULL,
  `numdiv` int(11) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fechamodificacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`iddivs`),
  KEY `fk_divs_configuraciones1` (`idconfiguraciones`),
  KEY `fk_divs_modulos1` (`tipomodulo`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `divs`
--

INSERT INTO `divs` (`iddivs`, `idconfiguraciones`, `tipomodulo`, `idmodulo`, `numdiv`, `estado`, `fechamodificacion`) VALUES
(9, 3, 2, 4, 1, 0, '2018-02-24 15:45:13'),
(10, 3, 4, 4, 2, 0, '2018-02-24 15:45:13'),
(11, 3, 3, 1, 3, 0, '2018-02-24 15:45:13'),
(12, 3, 3, 1, 4, 0, '2018-02-24 15:45:13'),
(13, 4, NULL, NULL, 1, 1, '2018-03-03 01:54:41'),
(14, 4, NULL, NULL, 2, 1, '2018-03-03 01:54:41'),
(15, 4, NULL, NULL, 3, 1, '2018-03-03 01:54:41'),
(16, 4, NULL, NULL, 4, 1, '2018-03-03 01:54:41');

--
-- Disparadores `divs`
--
DROP TRIGGER IF EXISTS `updateDiv`;
DELIMITER $$
CREATE TRIGGER `updateDiv` AFTER UPDATE ON `divs` FOR EACH ROW BEGIN
    DECLARE ides INT;    
    SELECT idesquemas FROM esquemas WHERE idconfiguraciones=OLD.idconfiguraciones INTO ides;
    UPDATE monitores SET refrescar = NOW() WHERE idesquemas=ides;      
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquemas`
--

DROP TABLE IF EXISTS `esquemas`;
CREATE TABLE IF NOT EXISTS `esquemas` (
  `idesquemas` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idconfiguraciones` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idesquemas`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`),
  KEY `fk_esquemas_instituciones1` (`idinstituciones`),
  KEY `fk_esquemas_configuraciones1` (`idconfiguraciones`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquemasusuarios`
--

DROP TABLE IF EXISTS `esquemasusuarios`;
CREATE TABLE IF NOT EXISTS `esquemasusuarios` (
  `idesquemas` int(11) NOT NULL,
  `idusuarios` int(11) NOT NULL,
  KEY `fk_esquemas_has_usuarios_usuarios1` (`idusuarios`),
  KEY `fk_esquemas_has_usuarios_esquemas1` (`idesquemas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagenes`
--

DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE IF NOT EXISTS `imagenes` (
  `idimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `idmoduloimagenes` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idimagenes`),
  KEY `fk_listaimagenes_moduloimagenes1` (`idmoduloimagenes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `imagenes`
--
DROP TRIGGER IF EXISTS `deleteImagen`;
DELIMITER $$
CREATE TRIGGER `deleteImagen` BEFORE DELETE ON `imagenes` FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=OLD.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertImagen`;
DELIMITER $$
CREATE TRIGGER `insertImagen` AFTER INSERT ON `imagenes` FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloimagenes FROM imagenes WHERE idimagenes=NEW.idimagenes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=2;   
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `instituciones`
--

DROP TABLE IF EXISTS `instituciones`;
CREATE TABLE IF NOT EXISTS `instituciones` (
  `idinstituciones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `direccion` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `estilo` varchar(45) DEFAULT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idinstituciones`),
  UNIQUE KEY `nombre_UNIQUE` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `instituciones`
--

INSERT INTO `instituciones` (`idinstituciones`, `nombre`, `direccion`, `telefono`, `logo`, `estilo`, `fecharegistro`) VALUES
(1, 'ninguna', '', '', '', NULL, '2013-04-30 13:55:21'),
(2, 'Universidad TecnolÃ³gica de Cancun', 'xxx', 'xxx', 'xxxx', 'defaul', '2013-04-30 13:55:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduloimagenes`
--

DROP TABLE IF EXISTS `moduloimagenes`;
CREATE TABLE IF NOT EXISTS `moduloimagenes` (
  `idmoduloimagenes` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmoduloimagenes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulonoticias`
--

DROP TABLE IF EXISTS `modulonoticias`;
CREATE TABLE IF NOT EXISTS `modulonoticias` (
  `idmodulonoticias` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `efecto` varchar(15) DEFAULT NULL,
  `tiempotransicion` int(11) DEFAULT NULL,
  PRIMARY KEY (`idmodulonoticias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulos`
--

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE IF NOT EXISTS `modulos` (
  `idmodulos` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `tabla` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmodulos`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `modulos`
--

INSERT INTO `modulos` (`idmodulos`, `nombre`, `tabla`, `fecharegistro`) VALUES
(1, 'videos', 'modulovideos', '2013-04-30 14:11:54'),
(2, 'imagenes', 'moduloimagenes', '2013-04-30 14:11:54'),
(3, 'noticias', 'modulonoticias', '2013-04-30 14:12:09'),
(4, 'Youtube', 'moduloyoutubes', '2018-02-24 18:45:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `modulovideos`
--

DROP TABLE IF EXISTS `modulovideos`;
CREATE TABLE IF NOT EXISTS `modulovideos` (
  `idmodulovideos` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechamodificacion` timestamp NULL DEFAULT NULL,
  `aleatorio` tinyint(1) DEFAULT NULL,
  `repetir` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idmodulovideos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `moduloyoutube`
--

DROP TABLE IF EXISTS `moduloyoutube`;
CREATE TABLE IF NOT EXISTS `moduloyoutube` (
  `idmoduloyoutube` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  PRIMARY KEY (`idmoduloyoutube`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `monitores`
--

DROP TABLE IF EXISTS `monitores`;
CREATE TABLE IF NOT EXISTS `monitores` (
  `idmonitores` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `idesquemas` int(11) DEFAULT NULL,
  `ip` varchar(15) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `marca` varchar(45) DEFAULT NULL,
  `modelo` varchar(45) DEFAULT NULL,
  `refrescar` timestamp NULL DEFAULT NULL,
  `ultimavista` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmonitores`),
  KEY `fk_monitores_instituciones1` (`idinstituciones`),
  KEY `fk_monitores_esquemas1` (`idesquemas`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
  `idnoticias` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulonoticias` int(11) NOT NULL,
  `titulo` varchar(45) NOT NULL,
  `texto` text NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idnoticias`),
  KEY `fk_noticias_modulonoticias1` (`idmodulonoticias`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `noticias`
--
DROP TRIGGER IF EXISTS `deleteNoticia`;
DELIMITER $$
CREATE TRIGGER `deleteNoticia` BEFORE DELETE ON `noticias` FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=OLD.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertNoticia`;
DELIMITER $$
CREATE TRIGGER `insertNoticia` AFTER INSERT ON `noticias` FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmodulonoticias FROM noticias WHERE idnoticias=NEW.idnoticias INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=3;   
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

DROP TABLE IF EXISTS `registros`;
CREATE TABLE IF NOT EXISTS `registros` (
  `idregistros` int(11) NOT NULL AUTO_INCREMENT,
  `idusuarios` int(11) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `accion` varchar(45) NOT NULL,
  `tabla` varchar(45) NOT NULL,
  `id` int(11) DEFAULT NULL,
  PRIMARY KEY (`idregistros`),
  KEY `fk_registros_usuarios1` (`idusuarios`)
) ENGINE=InnoDB AUTO_INCREMENT=221 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`idregistros`, `idusuarios`, `ip`, `fecha`, `accion`, `tabla`, `id`) VALUES
(1, 1, '::1', '2018-02-24 20:42:59', 'insert', 'moduloyoutube', 0),
(2, 1, '::1', '2018-02-24 20:43:04', 'insert', 'youtubes', 0),
(3, 1, '::1', '2018-02-24 20:43:11', 'insert', 'youtubes', 0),
(4, 1, '::1', '2018-03-03 01:51:25', 'insert', 'monitores', 0),
(5, 1, '::1', '2018-03-03 01:54:41', 'insert', 'configuraciones', 0),
(6, 1, '::1', '2018-03-03 01:54:41', 'insert', 'esquemas', 0),
(7, 1, '::1', '2018-03-03 01:54:46', 'update', 'monitores', 3),
(8, 1, '::1', '2018-03-12 00:53:21', 'update', 'monitores', 3),
(9, 1, '::1', '2018-03-12 00:53:22', 'update', 'monitores', 3),
(10, 1, '::1', '2018-03-12 00:53:22', 'update', 'monitores', 3),
(11, 1, '::1', '2018-03-12 00:53:22', 'update', 'monitores', 3),
(12, 1, '::1', '2018-03-12 00:58:00', 'update', 'monitores', 3),
(13, 1, '::1', '2018-03-12 00:58:00', 'update', 'monitores', 3),
(14, 1, '::1', '2018-03-12 00:58:01', 'update', 'monitores', 3),
(15, 1, '::1', '2018-03-12 00:58:01', 'update', 'monitores', 3),
(16, 1, '::1', '2018-03-12 00:58:02', 'update', 'monitores', 3),
(17, 1, '::1', '2018-03-12 00:58:02', 'update', 'monitores', 3),
(18, 1, '::1', '2018-03-12 00:58:03', 'update', 'monitores', 3),
(19, 1, '::1', '2018-03-12 00:58:04', 'update', 'monitores', 3),
(20, 1, '::1', '2018-03-12 00:58:04', 'update', 'monitores', 3),
(21, 1, '::1', '2018-03-12 00:58:13', 'update', 'monitores', 3),
(22, 1, '::1', '2018-03-12 00:58:21', 'update', 'monitores', 3),
(23, 1, '::1', '2018-03-12 00:58:22', 'update', 'monitores', 3),
(24, 1, '::1', '2018-03-12 00:58:22', 'update', 'monitores', 3),
(25, 1, '::1', '2018-03-12 00:58:34', 'update', 'monitores', 3),
(26, 1, '::1', '2018-03-12 00:58:35', 'update', 'monitores', 3),
(27, 1, '::1', '2018-03-12 00:58:35', 'update', 'monitores', 3),
(28, 1, '::1', '2018-03-12 00:58:35', 'update', 'monitores', 3),
(29, 1, '::1', '2018-03-12 00:59:01', 'update', 'monitores', 3),
(30, 1, '::1', '2018-03-12 00:59:02', 'update', 'monitores', 3),
(31, 1, '::1', '2018-03-12 00:59:02', 'update', 'monitores', 3),
(32, 1, '::1', '2018-03-12 00:59:02', 'update', 'monitores', 3),
(33, 1, '::1', '2018-03-12 00:59:02', 'update', 'monitores', 3),
(34, 1, '::1', '2018-03-12 00:59:09', 'update', 'monitores', 3),
(35, 1, '::1', '2018-03-12 01:00:12', 'update', 'monitores', 3),
(36, 1, '::1', '2018-03-12 01:00:35', 'update', 'monitores', 3),
(37, 1, '::1', '2018-03-12 01:02:27', 'update', 'monitores', 3),
(38, 1, '::1', '2018-03-12 01:02:31', 'update', 'monitores', 3),
(39, 1, '::1', '2018-03-12 01:04:38', 'update', 'monitores', 3),
(40, 1, '::1', '2018-03-12 01:12:05', 'update', 'monitores', 3),
(41, 1, '::1', '2018-03-12 01:12:06', 'update', 'monitores', 3),
(42, 1, '::1', '2018-03-12 01:12:14', 'update', 'monitores', 3),
(43, 1, '::1', '2018-03-12 01:12:37', 'update', 'monitores', 3),
(44, 1, '::1', '2018-03-12 01:12:48', 'update', 'monitores', 3),
(45, 1, '::1', '2018-03-12 01:16:18', 'update', 'monitores', 3),
(46, 1, '::1', '2018-03-12 01:16:28', 'update', 'monitores', 3),
(47, 1, '::1', '2018-03-12 01:16:38', 'update', 'monitores', 3),
(48, 1, '::1', '2018-03-12 01:16:48', 'update', 'monitores', 3),
(49, 1, '::1', '2018-03-12 01:16:58', 'update', 'monitores', 3),
(50, 1, '::1', '2018-03-12 01:17:08', 'update', 'monitores', 3),
(51, 1, '::1', '2018-03-12 01:17:18', 'update', 'monitores', 3),
(52, 1, '::1', '2018-03-12 01:17:28', 'update', 'monitores', 3),
(53, 1, '::1', '2018-03-12 01:17:38', 'update', 'monitores', 3),
(54, 1, '::1', '2018-03-12 01:17:48', 'update', 'monitores', 3),
(55, 1, '::1', '2018-03-12 01:17:58', 'update', 'monitores', 3),
(56, 1, '::1', '2018-03-12 01:18:08', 'update', 'monitores', 3),
(57, 1, '::1', '2018-03-12 01:18:18', 'update', 'monitores', 3),
(58, 1, '::1', '2018-03-12 01:18:28', 'update', 'monitores', 3),
(59, 1, '::1', '2018-03-12 01:18:35', 'update', 'monitores', 3),
(60, 1, '::1', '2018-03-12 01:18:45', 'update', 'monitores', 3),
(61, 1, '::1', '2018-03-12 01:18:55', 'update', 'monitores', 3),
(62, 1, '::1', '2018-03-12 01:19:05', 'update', 'monitores', 3),
(63, 1, '::1', '2018-03-12 01:19:15', 'update', 'monitores', 3),
(64, 1, '::1', '2018-03-12 01:19:25', 'update', 'monitores', 3),
(65, 1, '::1', '2018-03-12 01:19:27', 'update', 'monitores', 3),
(66, 1, '::1', '2018-03-12 01:19:27', 'update', 'monitores', 3),
(67, 1, '::1', '2018-03-12 01:19:28', 'update', 'monitores', 3),
(68, 1, '::1', '2018-03-12 01:19:28', 'update', 'monitores', 3),
(69, 1, '::1', '2018-03-12 01:19:32', 'update', 'monitores', 3),
(70, 1, '::1', '2018-03-12 01:19:33', 'update', 'monitores', 3),
(71, 1, '::1', '2018-03-12 01:19:35', 'update', 'monitores', 3),
(72, 1, '::1', '2018-03-12 01:19:45', 'update', 'monitores', 3),
(73, 1, '::1', '2018-03-12 01:20:22', 'update', 'monitores', 3),
(74, 1, '::1', '2018-03-12 01:20:22', 'update', 'monitores', 3),
(75, 1, '::1', '2018-03-12 01:20:22', 'update', 'monitores', 3),
(76, 1, '::1', '2018-03-12 01:20:22', 'update', 'monitores', 3),
(77, 1, '::1', '2018-03-12 01:20:23', 'update', 'monitores', 3),
(78, 1, '::1', '2018-03-12 01:20:25', 'update', 'monitores', 3),
(79, 1, '::1', '2018-03-12 01:20:35', 'update', 'monitores', 3),
(80, 1, '::1', '2018-03-12 01:20:36', 'update', 'monitores', 3),
(81, 1, '::1', '2018-03-12 01:20:45', 'update', 'monitores', 3),
(82, 1, '::1', '2018-03-12 01:20:55', 'update', 'monitores', 3),
(83, 1, '::1', '2018-03-12 01:21:05', 'update', 'monitores', 3),
(84, 1, '::1', '2018-03-12 01:21:15', 'update', 'monitores', 3),
(85, 1, '::1', '2018-03-12 01:21:25', 'update', 'monitores', 3),
(86, 1, '::1', '2018-03-12 01:21:35', 'update', 'monitores', 3),
(87, 1, '::1', '2018-03-12 01:21:45', 'update', 'monitores', 3),
(88, 1, '::1', '2018-03-12 01:21:55', 'update', 'monitores', 3),
(89, 1, '::1', '2018-03-12 01:22:05', 'update', 'monitores', 3),
(90, 1, '::1', '2018-03-12 01:22:07', 'update', 'monitores', 3),
(91, 1, '::1', '2018-03-12 01:22:08', 'update', 'monitores', 3),
(92, 1, '::1', '2018-03-12 01:22:15', 'update', 'monitores', 3),
(93, 1, '::1', '2018-03-12 01:22:16', 'update', 'monitores', 3),
(94, 1, '::1', '2018-03-12 01:22:16', 'update', 'monitores', 3),
(95, 1, '::1', '2018-03-12 01:22:16', 'update', 'monitores', 3),
(96, 1, '::1', '2018-03-12 01:22:25', 'update', 'monitores', 3),
(97, 1, '::1', '2018-03-12 01:28:49', 'update', 'monitores', 3),
(98, 1, '::1', '2018-03-12 01:28:50', 'update', 'monitores', 3),
(99, 1, '::1', '2018-03-12 01:29:19', 'update', 'monitores', 3),
(100, 1, '::1', '2018-03-12 01:29:19', 'update', 'monitores', 3),
(101, 1, '::1', '2018-03-12 01:29:20', 'update', 'monitores', 3),
(102, 1, '::1', '2018-03-12 01:29:20', 'update', 'monitores', 3),
(103, 1, '::1', '2018-03-12 01:29:20', 'update', 'monitores', 3),
(104, 1, '::1', '2018-03-12 01:29:21', 'update', 'monitores', 3),
(105, 1, '::1', '2018-03-12 01:29:22', 'update', 'monitores', 3),
(106, 1, '::1', '2018-03-12 01:29:22', 'update', 'monitores', 3),
(107, 1, '::1', '2018-03-12 01:29:22', 'update', 'monitores', 3),
(108, 1, '::1', '2018-03-12 01:29:23', 'update', 'monitores', 3),
(109, 1, '::1', '2018-03-12 01:29:24', 'update', 'monitores', 3),
(110, 1, '::1', '2018-03-12 01:29:30', 'update', 'monitores', 3),
(111, 1, '::1', '2018-03-12 01:29:40', 'update', 'monitores', 3),
(112, 1, '::1', '2018-03-12 01:29:50', 'update', 'monitores', 3),
(113, 1, '::1', '2018-03-12 01:29:54', 'update', 'monitores', 3),
(114, 1, '::1', '2018-03-12 01:30:00', 'update', 'monitores', 3),
(115, 1, '::1', '2018-03-12 01:30:10', 'update', 'monitores', 3),
(116, 1, '::1', '2018-03-12 01:30:20', 'update', 'monitores', 3),
(117, 1, '::1', '2018-03-12 01:30:30', 'update', 'monitores', 3),
(118, 1, '::1', '2018-03-12 01:30:40', 'update', 'monitores', 3),
(119, 1, '::1', '2018-03-12 01:30:50', 'update', 'monitores', 3),
(120, 1, '::1', '2018-03-12 01:31:00', 'update', 'monitores', 3),
(121, 1, '::1', '2018-03-12 01:31:10', 'update', 'monitores', 3),
(122, 1, '::1', '2018-03-12 01:31:20', 'update', 'monitores', 3),
(123, 1, '::1', '2018-03-12 01:31:30', 'update', 'monitores', 3),
(124, 1, '::1', '2018-03-12 01:31:41', 'update', 'monitores', 3),
(125, 1, '::1', '2018-03-12 01:31:51', 'update', 'monitores', 3),
(126, 1, '::1', '2018-03-12 01:32:01', 'update', 'monitores', 3),
(127, 1, '::1', '2018-03-12 01:32:11', 'update', 'monitores', 3),
(128, 1, '::1', '2018-03-12 01:32:21', 'update', 'monitores', 3),
(129, 1, '::1', '2018-03-12 01:32:31', 'update', 'monitores', 3),
(130, 1, '::1', '2018-03-12 01:32:41', 'update', 'monitores', 3),
(131, 1, '::1', '2018-03-12 01:32:51', 'update', 'monitores', 3),
(132, 1, '::1', '2018-03-12 01:33:01', 'update', 'monitores', 3),
(133, 1, '::1', '2018-03-12 01:33:11', 'update', 'monitores', 3),
(134, 1, '::1', '2018-03-12 01:33:21', 'update', 'monitores', 3),
(135, 1, '::1', '2018-03-12 01:33:31', 'update', 'monitores', 3),
(136, 1, '::1', '2018-03-12 01:33:41', 'update', 'monitores', 3),
(137, 1, '::1', '2018-03-12 01:33:51', 'update', 'monitores', 3),
(138, 1, '::1', '2018-03-12 01:34:01', 'update', 'monitores', 3),
(139, 1, '::1', '2018-03-12 01:34:11', 'update', 'monitores', 3),
(140, 1, '::1', '2018-03-12 01:34:21', 'update', 'monitores', 3),
(141, 1, '::1', '2018-03-12 01:34:31', 'update', 'monitores', 3),
(142, 1, '::1', '2018-03-12 01:34:41', 'update', 'monitores', 3),
(143, 1, '::1', '2018-03-12 01:34:51', 'update', 'monitores', 3),
(144, 1, '::1', '2018-03-12 01:35:01', 'update', 'monitores', 3),
(145, 1, '::1', '2018-03-12 01:35:11', 'update', 'monitores', 3),
(146, 1, '::1', '2018-03-12 01:35:21', 'update', 'monitores', 3),
(147, 1, '::1', '2018-03-12 01:35:31', 'update', 'monitores', 3),
(148, 1, '::1', '2018-03-12 01:35:41', 'update', 'monitores', 3),
(149, 1, '::1', '2018-03-12 01:35:51', 'update', 'monitores', 3),
(150, 1, '::1', '2018-03-12 01:36:01', 'update', 'monitores', 3),
(151, 1, '::1', '2018-03-12 01:36:10', 'update', 'monitores', 3),
(152, 1, '::1', '2018-03-12 01:36:21', 'update', 'monitores', 3),
(153, 1, '::1', '2018-03-12 01:36:31', 'update', 'monitores', 3),
(154, 1, '::1', '2018-03-12 01:36:41', 'update', 'monitores', 3),
(155, 1, '::1', '2018-03-12 01:36:51', 'update', 'monitores', 3),
(156, 1, '::1', '2018-03-12 01:37:00', 'update', 'monitores', 3),
(157, 1, '::1', '2018-03-12 01:37:11', 'update', 'monitores', 3),
(158, 1, '::1', '2018-03-12 01:37:21', 'update', 'monitores', 3),
(159, 1, '::1', '2018-03-12 01:37:31', 'update', 'monitores', 3),
(160, 1, '::1', '2018-03-12 01:37:41', 'update', 'monitores', 3),
(161, 1, '::1', '2018-03-12 01:37:51', 'update', 'monitores', 3),
(162, 1, '::1', '2018-03-12 01:38:01', 'update', 'monitores', 3),
(163, 1, '::1', '2018-03-12 01:38:11', 'update', 'monitores', 3),
(164, 1, '::1', '2018-03-12 01:38:21', 'update', 'monitores', 3),
(165, 1, '::1', '2018-03-12 01:38:31', 'update', 'monitores', 3),
(166, 1, '::1', '2018-03-12 01:38:41', 'update', 'monitores', 3),
(167, 1, '::1', '2018-03-12 01:38:51', 'update', 'monitores', 3),
(168, 1, '::1', '2018-03-12 01:39:01', 'update', 'monitores', 3),
(169, 1, '::1', '2018-03-12 01:39:11', 'update', 'monitores', 3),
(170, 1, '::1', '2018-03-12 01:39:21', 'update', 'monitores', 3),
(171, 1, '::1', '2018-03-12 01:39:31', 'update', 'monitores', 3),
(172, 1, '::1', '2018-03-12 01:39:41', 'update', 'monitores', 3),
(173, 1, '::1', '2018-03-12 01:39:51', 'update', 'monitores', 3),
(174, 1, '::1', '2018-03-12 01:40:01', 'update', 'monitores', 3),
(175, 1, '::1', '2018-03-12 01:40:11', 'update', 'monitores', 3),
(176, 1, '::1', '2018-03-12 01:40:21', 'update', 'monitores', 3),
(177, 1, '::1', '2018-03-12 01:40:31', 'update', 'monitores', 3),
(178, 1, '::1', '2018-03-12 01:40:41', 'update', 'monitores', 3),
(179, 1, '::1', '2018-03-12 01:40:48', 'update', 'monitores', 3),
(180, 1, '::1', '2018-03-12 01:40:58', 'update', 'monitores', 3),
(181, 1, '::1', '2018-03-12 01:41:08', 'update', 'monitores', 3),
(182, 1, '::1', '2018-03-12 01:41:18', 'update', 'monitores', 3),
(183, 1, '::1', '2018-03-12 01:41:28', 'update', 'monitores', 3),
(184, 1, '::1', '2018-03-12 01:41:38', 'update', 'monitores', 3),
(185, 1, '::1', '2018-03-12 01:41:48', 'update', 'monitores', 3),
(186, 1, '::1', '2018-03-12 01:41:58', 'update', 'monitores', 3),
(187, 1, '::1', '2018-03-12 01:42:08', 'update', 'monitores', 3),
(188, 1, '::1', '2018-03-12 01:42:18', 'update', 'monitores', 3),
(189, 1, '::1', '2018-03-12 01:42:28', 'update', 'monitores', 3),
(190, 1, '::1', '2018-03-12 01:42:33', 'update', 'monitores', 3),
(191, 1, '::1', '2018-03-12 01:42:38', 'update', 'monitores', 3),
(192, 1, '::1', '2018-03-12 01:42:38', 'update', 'monitores', 3),
(193, 1, '::1', '2018-03-12 01:42:48', 'update', 'monitores', 3),
(194, 1, '::1', '2018-03-12 01:42:55', 'update', 'monitores', 3),
(195, 1, '::1', '2018-03-12 01:42:58', 'update', 'monitores', 3),
(196, 1, '::1', '2018-03-12 01:42:58', 'update', 'monitores', 3),
(197, 1, '::1', '2018-03-12 01:43:09', 'update', 'monitores', 3),
(198, 1, '::1', '2018-03-12 01:43:19', 'update', 'monitores', 3),
(199, 1, '::1', '2018-03-12 01:43:29', 'update', 'monitores', 3),
(200, 1, '::1', '2018-03-12 01:43:39', 'update', 'monitores', 3),
(201, 1, '::1', '2018-03-12 01:43:49', 'update', 'monitores', 3),
(202, 1, '::1', '2018-03-12 01:43:58', 'update', 'monitores', 3),
(203, 1, '::1', '2018-03-12 01:43:59', 'update', 'monitores', 3),
(204, 1, '::1', '2018-03-12 01:44:09', 'update', 'monitores', 3),
(205, 1, '::1', '2018-03-12 01:44:19', 'update', 'monitores', 3),
(206, 1, '::1', '2018-03-12 01:44:29', 'update', 'monitores', 3),
(207, 1, '::1', '2018-03-12 01:44:39', 'update', 'monitores', 3),
(208, 1, '::1', '2018-03-12 01:44:49', 'update', 'monitores', 3),
(209, 1, '::1', '2018-03-12 01:44:59', 'update', 'monitores', 3),
(210, 1, '::1', '2018-03-12 01:45:09', 'update', 'monitores', 3),
(211, 1, '::1', '2018-03-12 01:45:19', 'update', 'monitores', 3),
(212, 1, '::1', '2018-03-12 01:45:29', 'update', 'monitores', 3),
(213, 1, '::1', '2018-03-12 01:45:39', 'update', 'monitores', 3),
(214, 1, '::1', '2018-03-12 01:45:49', 'update', 'monitores', 3),
(215, 1, '::1', '2018-03-12 01:46:00', 'insert', 'monitores', 0),
(216, 1, '::1', '2018-03-12 01:46:10', 'insert', 'monitores', 0),
(217, 1, '::1', '2018-03-12 01:46:20', 'insert', 'monitores', 0),
(218, 1, '::1', '2018-03-12 01:46:31', 'insert', 'monitores', 0),
(219, 1, '::1', '2018-03-12 01:46:41', 'insert', 'monitores', 0),
(220, 1, '::1', '2018-03-12 01:46:51', 'insert', 'monitores', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `templates`
--

DROP TABLE IF EXISTS `templates`;
CREATE TABLE IF NOT EXISTS `templates` (
  `idtemplates` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(45) NOT NULL,
  `url` varchar(45) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `numdiv` int(11) NOT NULL,
  PRIMARY KEY (`idtemplates`),
  UNIQUE KEY `titulo_UNIQUE` (`titulo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `templates`
--

INSERT INTO `templates` (`idtemplates`, `titulo`, `url`, `estado`, `numdiv`) VALUES
(1, 'plantilla1', 'uploads/plantillas/plantilla1/', 1, 4),
(2, 'plantilla2', 'uploads/plantillas/plantilla2/', 1, 5),
(3, 'plantilla3', 'uploads/plantillas/plantilla3/', 1, 5),
(4, 'plantilla4', 'uploads/plantillas/plantilla4/', 1, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE IF NOT EXISTS `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `idinstituciones` int(11) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `nivel` tinyint(4) NOT NULL,
  `estado` tinyint(1) NOT NULL DEFAULT '1',
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fechaacceso` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idusuarios`),
  UNIQUE KEY `usuario_UNIQUE` (`usuario`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_usuarios_instituciones1` (`idinstituciones`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuarios`, `idinstituciones`, `nombre`, `email`, `usuario`, `password`, `nivel`, `estado`, `fecharegistro`, `fechaacceso`) VALUES
(1, 1, 'administrador', '', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 0, 1, '2013-04-30 13:56:40', '2018-03-12 17:47:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

DROP TABLE IF EXISTS `videos`;
CREATE TABLE IF NOT EXISTS `videos` (
  `idvideos` int(11) NOT NULL AUTO_INCREMENT,
  `idmodulovideos` int(11) NOT NULL,
  `titulo` varchar(45) DEFAULT NULL,
  `url` varchar(250) NOT NULL,
  `fechainicio` timestamp NULL DEFAULT NULL,
  `fechafinal` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idvideos`),
  KEY `fk_listavideos_modulovideos1` (`idmodulovideos`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `videos`
--
DROP TRIGGER IF EXISTS `deleteVideo`;
DELIMITER $$
CREATE TRIGGER `deleteVideo` BEFORE DELETE ON `videos` FOR EACH ROW BEGIN
    DECLARE idimg INT;    
    SELECT idmodulovideos FROM videos WHERE idvideos=OLD.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertVideo`;
DELIMITER $$
CREATE TRIGGER `insertVideo` AFTER INSERT ON `videos` FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmodulovideos FROM videos WHERE idvideos=NEW.idvideos INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=1;   
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `youtubes`
--

DROP TABLE IF EXISTS `youtubes`;
CREATE TABLE IF NOT EXISTS `youtubes` (
  `idyoutubes` int(11) NOT NULL AUTO_INCREMENT,
  `idmoduloyoutubes` int(11) NOT NULL,
  `url` varchar(45) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idyoutubes`),
  KEY `fk_youtubes_moduloyoutube1_idx` (`idmoduloyoutubes`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `youtubes`
--
DROP TRIGGER IF EXISTS `deleteYoutube`;
DELIMITER $$
CREATE TRIGGER `deleteYoutube` BEFORE DELETE ON `youtubes` FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloyoutubes FROM youtubes WHERE idyoutubes=OLD.idyoutubes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=4;   
END
$$
DELIMITER ;
DROP TRIGGER IF EXISTS `insertYoutube`;
DELIMITER $$
CREATE TRIGGER `insertYoutube` AFTER INSERT ON `youtubes` FOR EACH ROW BEGIN
    DECLARE idimg INT;   
    SELECT idmoduloyoutubes FROM youtubes WHERE idyoutubes=NEW.idyoutubes INTO idimg;
    UPDATE divs SET fechamodificacion=NOW() WHERE idmodulo=idimg AND tipomodulo=4;   
END
$$
DELIMITER ;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `configuraciones`
--
ALTER TABLE `configuraciones`
  ADD CONSTRAINT `fk_configuracionesquema_templates1` FOREIGN KEY (`idtemplates`) REFERENCES `templates` (`idtemplates`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `divs`
--
ALTER TABLE `divs`
  ADD CONSTRAINT `fk_divs_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_divs_modulos1` FOREIGN KEY (`tipomodulo`) REFERENCES `modulos` (`idmodulos`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Filtros para la tabla `esquemas`
--
ALTER TABLE `esquemas`
  ADD CONSTRAINT `fk_esquemas_configuraciones1` FOREIGN KEY (`idconfiguraciones`) REFERENCES `configuraciones` (`idconfiguraciones`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_esquemas_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `esquemasusuarios`
--
ALTER TABLE `esquemasusuarios`
  ADD CONSTRAINT `fk_esquemas_has_usuarios_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_esquemas_has_usuarios_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `imagenes`
--
ALTER TABLE `imagenes`
  ADD CONSTRAINT `fk_listaimagenes_moduloimagenes1` FOREIGN KEY (`idmoduloimagenes`) REFERENCES `moduloimagenes` (`idmoduloimagenes`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `monitores`
--
ALTER TABLE `monitores`
  ADD CONSTRAINT `fk_monitores_esquemas1` FOREIGN KEY (`idesquemas`) REFERENCES `esquemas` (`idesquemas`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_monitores_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `fk_noticias_modulonoticias1` FOREIGN KEY (`idmodulonoticias`) REFERENCES `modulonoticias` (`idmodulonoticias`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `registros`
--
ALTER TABLE `registros`
  ADD CONSTRAINT `fk_registros_usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_instituciones1` FOREIGN KEY (`idinstituciones`) REFERENCES `instituciones` (`idinstituciones`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk_listavideos_modulovideos1` FOREIGN KEY (`idmodulovideos`) REFERENCES `modulovideos` (`idmodulovideos`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Filtros para la tabla `youtubes`
--
ALTER TABLE `youtubes`
  ADD CONSTRAINT `fk_youtubes_moduloyoutube1` FOREIGN KEY (`idmoduloyoutubes`) REFERENCES `moduloyoutube` (`idmoduloyoutube`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
